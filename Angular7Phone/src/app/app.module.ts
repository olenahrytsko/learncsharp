import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from "@angular/forms"
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 
import { ToastrModule } from 'ngx-toastr';
import { AppComponent } from './app.component';
import { PersonDetailsComponent } from './person-details/person-details.component';
import { PersonDetailComponent } from './person-details/person-detail/person-detail.component';
import { PersonDetailListComponent } from './person-details/person-detail-list/person-detail-list.component';
import { PersonDetailService } from './shared/person-detail.service';
import { CityDetailComponent } from './person-details/city-detail/city-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    PersonDetailsComponent,
    PersonDetailComponent,
    PersonDetailListComponent,
    CityDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
    
  ],
  providers: [PersonDetailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
