import { CityDetail } from './city-detail.model';

export class PersonDetail {
     Name: string;
     Surname:string;
     Nickname: string;
     PhoneNumber:string;
     PersonId:number;
     City:string;
     Cities:CityDetail;
}
