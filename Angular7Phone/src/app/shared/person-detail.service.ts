import { PersonDetail } from './person-detail.model';
import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { CityDetail } from './city-detail.model';
@Injectable({
  providedIn: 'root'
})
export class PersonDetailService {
  formData: PersonDetail;
  formCity: CityDetail;
  list : PersonDetail[];
  listCity:CityDetail[];
  readonly rootURL = 'http://localhost:51523/api'; 

  constructor(private http: HttpClient) { }

  postPersonDetail() {
    return this.http.post(this.rootURL + '/People', this.formData);
  }
  putPersonDetail() {
    return this.http.put(this.rootURL + '/People/'+ this.formData.PersonId, this.formData);
  }
  deletePersonDetail(id) {
    return this.http.delete(this.rootURL + '/People/'+ id);
  }
  postCityDetail() {
    return this.http.post(this.rootURL + '/Cities', this.formData.City);
  }

  refreshList(){
    this.http.get(this.rootURL + '/People')
    .toPromise()
    .then(res => this.list = res as PersonDetail[]);
  
  }
}
