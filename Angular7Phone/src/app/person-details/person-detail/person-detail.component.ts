
import { Component, OnInit } from '@angular/core';
import { PersonDetailService } from 'src/app/shared/person-detail.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-person-detail',
  templateUrl: './person-detail.component.html',
  styles: []
})
export class PersonDetailComponent implements OnInit {

  constructor(private service:PersonDetailService,
    private toastr:ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }
  resetForm(form?:NgForm){
    if(form!=null)
    form.resetForm();
    this.service.formData={
     PersonId:0,
     Name:'',
     Surname:'',
     Nickname:'',
     PhoneNumber:'',
     City:'',
     Cities:null
    }
  }
  onSubmit(form:NgForm){
    if(this.service.formData.PersonId==0)
    this.insertRecordPerson(form)
     else this.updateRecord(form);
  }
  insertRecordPerson(form:NgForm){
    this.service.postPersonDetail().subscribe(
      res =>{
        this.resetForm(form);
        this.toastr.success('Submitted succesfully','Person PhoneBook');
        this.service.refreshList();
      },
      err =>{
        console.log(err);
      }
    );
    }
updateRecord(form:NgForm){
    this.service.putPersonDetail().subscribe(
      res =>{
        this.resetForm(form);
        this.toastr.info('Submitted succesfully','Person PhoneBook');
        this.service.refreshList();
      },
      err =>{
        console.log(err);
      }
    );
  }
}