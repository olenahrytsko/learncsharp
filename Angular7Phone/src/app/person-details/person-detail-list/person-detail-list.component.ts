import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { PersonDetail } from 'src/app/shared/person-detail.model';
import { PersonDetailService } from 'src/app/shared/person-detail.service';
import { CityDetail } from 'src/app/shared/city-detail.model';

@Component({
  selector: 'app-person-detail-list',
  templateUrl: './person-detail-list.component.html',
  styles: []
})
export class PersonDetailListComponent implements OnInit {

  constructor(private service:PersonDetailService,
    private toastr:ToastrService) { }

  ngOnInit() {
    this.service.refreshList();
  }
  populateForm(pd:PersonDetail){
this.service.formData=Object.assign({},pd);
  }

  onDelete(PersonId){
    if(confirm('Are you sure to delete this record?'))
this.service.deletePersonDetail(PersonId)
.subscribe(
  res =>{
    this.service.refreshList();
    this.toastr.warning('Deleted successfully','Person PhoneBook');
  },
  err =>{
    console.log(err);
  }
)
  }
}
