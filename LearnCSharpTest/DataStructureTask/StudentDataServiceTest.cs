﻿using System;
using LearnCSharp.DataStructureTask;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LearnCSharpTest.DataStructureTask
{
    [TestClass]
    public class StudentDataServiceTest
    {
        [TestMethod]
        public void PrintCoursesStudents()
        {
            var data = StudentDataService.BuildCourse();
            StudentDataService.ConsoleOutput(data);
        }
    }
}