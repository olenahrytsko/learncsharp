﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using LearnCSharp.DataStructureTask;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LearnCSharpTest.DataStructureTask
{
    [TestClass]
    public class ConferenceHallTest
    {
        private List<TimeTable> CreateListConference()
        {
            var list = new List<TimeTable>
            {
                new TimeTable()
                {
                    StartDateTime = new DateTime(2018, 3, 9, 12, 10, 0),
                    EndDateTime = new DateTime(2018, 3, 9, 12, 20, 0),
                    Name = "A3"
                },
                new TimeTable()
                {
                    StartDateTime = new DateTime(2018, 3, 9, 12, 12, 0),
                    EndDateTime = new DateTime(2018, 3, 9, 12, 15, 0),
                    Name = "A4"
                },
                new TimeTable()
                {
                    StartDateTime = new DateTime(2018, 3, 9, 12, 13, 0),
                    EndDateTime = new DateTime(2018, 3, 9, 12, 16, 0),
                    Name = "A1"
                },
                new TimeTable()
                {
                    StartDateTime = new DateTime(2018, 3, 9, 14, 10, 0),
                    EndDateTime = new DateTime(2018, 3, 9, 15, 0, 0),
                    Name = "A2"
                },
                new TimeTable()
                {
                    StartDateTime = new DateTime(2018, 3, 8, 11, 10, 0),
                    EndDateTime = new DateTime(2018, 3, 8, 12, 0, 0),
                    Name = "A2"
                }
            };
            return list;
        }

        [TestMethod]
        public void GetSortedSetAvailableHall()
        {
            var list = CreateListConference();
            var conference = new ConferenceHall(list);
            var start = new TimeTable()
            {
                StartDateTime = new DateTime(2018, 3, 9, 12, 10, 0), EndDateTime = new DateTime(2018, 3, 9, 12, 20, 0)
            };
            var hall = conference.GetHall(start);
            var expected=new SortedSet<string>(){"A2"};
            Assert.AreEqual(5,list.Count);
            CollectionAssert.AreEqual(expected,hall);
        }
    }
}