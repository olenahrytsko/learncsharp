﻿using System.Collections.Generic;
using LearnCSharp.DataStructureTask;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LearnCSharpTest.DataStructureTask
{
    [TestClass]
    public class BiDictionaryTest
    {


        [TestMethod]
        public void FindCompositeKey()
        {
            var list = new List<int>() {10, 0};
            var biDict = CreateTestDictionary();
            var valFind = biDict.Find(5, "3");
            CollectionAssert.AreEqual(valFind, list);
        }

        [TestMethod]
        public void FindKey1()
        {
            var list = new List<int>() { 10, 0, 132 };
            var biDict = CreateTestDictionary();
            var valFind = biDict.Find(5, null);
            CollectionAssert.AreEqual(valFind, list);
        }

        [TestMethod]
        public void FindKey2()
        {
            var list = new List<int>() { 10, 0, 1 };
            var biDict = CreateTestDictionary();
            var valFind = biDict.Find(null, "3");
            CollectionAssert.AreEqual(valFind, list);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void KeyNotFound()
        {
            var biDict = new BiDictionary<int?, string, int>();
            biDict.AddTriples(5, "3", 10);
            biDict.AddTriples(1, "10", 12);
            var valFind = biDict.Find(null, "2");
        }

        private static BiDictionary<int?, string, int> CreateTestDictionary()
        {
            var biDict = new BiDictionary<int?, string, int>();
            biDict.AddTriples(5, "3", 10);
            biDict.AddTriples(1, "10", 12);
            biDict.AddTriples(5, "3", 0);
            biDict.AddTriples(2, "3", 1);
            biDict.AddTriples(5, "4", 132);
            biDict.AddTriples(8, null, 19);
            biDict.AddTriples(8, "1", 19);
            return biDict;
        }
    }
}