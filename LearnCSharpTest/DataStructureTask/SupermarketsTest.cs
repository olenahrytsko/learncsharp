﻿using System.Collections.Generic;
using LearnCSharp.DataStructureTask;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LearnCSharpTest.DataStructureTask
{
    [TestClass]
    public class SupermarketsTest
    {
        [TestMethod]
        public void PriceBetweenWithoutSamePrice()
        {
            var product = new Supermarket(CreateProducts(5));
            var price = new Product {Price = 10};
            var price2 = new Product {Price = 40};
            var sortedlist = product.GetProducts(price, price2, 10);
            var i = 0;
            foreach (var item in sortedlist)
            {
                i += 10;
                Assert.AreEqual(i, item.Price);
            }
        }

        private static IEnumerable<Product> CreateProducts(int count)
        {
            var j = 10;
            for (var i = 0; i < count; i++)
            {
                var product = new Product { Price = j };
                j = j + 10;
                product.Barcode = i;
                yield return product;
            }
        }
    }
}