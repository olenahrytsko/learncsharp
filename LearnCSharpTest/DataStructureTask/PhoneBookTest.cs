﻿using System.Collections.Generic;
using LearnCSharp;
using LearnCSharp.DataStructureTask;
using LearnCSharpEFCore.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LearnCSharpTest.DataStructureTask
{
    [TestClass]
    public class PhoneBookTest
    {
        [TestMethod]
        public void FindInPhoneBook()
        {
            var list = CreatePerson();
            var sameNameCity = CreatePersonWithTheSameNameCity();
            var sameSurName = CreatePersonWithTheSameSurName();
            var phoneBook = new PhoneBook(list);
            var byNameAndCity = phoneBook.FindPersons("Kevin", "Virginia beach");
            Assert.AreEqual(2, byNameAndCity.Count);
            for (var  i = 0; i < byNameAndCity.Count; i++)
            {
                byNameAndCity[i].PhoneNumber.Equals(sameNameCity[i].PhoneNumber);
            }

            var byName = phoneBook.FindPersons("Clark");
            for (var i = 0; i < byName.Count; i++)
            {
                byName[i].PhoneNumber.Equals(sameSurName[i].PhoneNumber);
            }
        }

        [TestMethod]
        public void IfPhoneBookSortedByCity()
        {
            var list = CreateSortedCity();
            var listPerson = CreatePerson();
            var phoneBook = new PhoneBook(listPerson);
            CollectionAssert.AreEqual(phoneBook.CityDictionary.Keys, list);
        }

        private static List<City> CreateSortedCity()
        {
            var list = new List<City>();
            list.Add(new City("Phoenix"));
            list.Add(new City("Portland"));
            list.Add(new City("San Antonio"));
            list.Add(new City("Virginia beach"));
            return list;
        }

        private static List<Person> CreatePersonWithTheSameSurName()
        {
            var list = new List<Person>();
            list.Add(new Person("Kevin", "Clark", null, "Virginia beach", "1-454-345-2345"));
            list.Add(new Person("Kevin", "Clark ", "Jones ", "Portland", "1-432-556-6533"));
            return list;
        }

        private static List<Person> CreatePersonWithTheSameNameCity()
        {
            var list = new List<Person>();
            list.Add(new Person("Kevin", "Clark", null, "Virginia beach", "1-454-345-2345"));
            list.Add(new Person("Kevin", "Garcia", null, "Virginia beach", "1-445-456-6732"));
            return list;
        }

        private static List<Person> CreatePerson()
        {
            var list = new List<Person>
            {
                new Person("Kevin", "Clark", null, "Virginia beach", "1-454-345-2345"),
                new Person("Skiller", null, null, "San Antonio", "1-566-533-2789"),
                new Person("Kevin", "Clark ", "Jones ", "Portland", "1-432-556-6533"),
                new Person("Linda", "Johnson", null, "San Antonio", "1-123-345-2456"),
                new Person("Kevin", null, null, "Phoenix", "1-564-254-4352"),
                new Person("Kevin", "Garcia", null, "Virginia beach", "1-445-456-6732"),
                new Person("Kevin", null, null, "Phoenix", "1-134-654-7424")
            };
            return list;

        }
    }
}