﻿using LearnCSharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LearnCSharpTest.Sort
{
    [TestClass]
    public class SortPersonAgeTest : SortingTestBase<PersonName>
    {
        public SortPersonAgeTest() : base(ListHelper.CreatePersonList, (x, y) => x.Age > y.Age, (x, y) => x.Age < y.Age)
        {
        }
    }
}

