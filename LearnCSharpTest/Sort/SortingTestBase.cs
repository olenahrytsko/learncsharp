﻿using System;
using System.Collections.Generic;
using LearnCSharp.Sort;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LearnCSharpTest.Sort
{
    [TestClass]
    public abstract class SortingTestBase<T>
    {
        private readonly Func<int, List<T>> getItems;
        private readonly Func<T, T, bool> comparer;
        private readonly Func<T, T, bool> descComparer;

        protected SortingTestBase(Func<int, List<T>> getItems, Func<T, T, bool> comparer, Func<T, T, bool> descComparer)
        {
            this.getItems = getItems;
            this.comparer = comparer;
            this.descComparer = descComparer;
        }

        [TestMethod]
        public void BubbleSortPersonTest()
        {
            SortAlgorithmTest(new BubbleSort<T>(comparer), comparer);
        }

        [TestMethod]
        public void BubbleSortDescendingPersonTest()
        {
            SortDescendingAlgorithmTest(new BubbleSort<T>(comparer), descComparer);
        }

        [TestMethod]
        public void InsertionSortTest()
        {
            SortAlgorithmTest(new InsertionSort<T>(comparer), comparer);
        }

        [TestMethod]
        public void InsertionSortDescendingTest()
        {
            SortDescendingAlgorithmTest(new InsertionSort<T>(comparer), descComparer);
        }

        [TestMethod]
        public void QuickSortTest()
        {
            SortAlgorithmTest(new QuickSort<T>(descComparer), comparer);
        }

        [TestMethod]
        public void QuickSortDescendingTest()
        {
            SortDescendingAlgorithmTest(new QuickSort<T>(descComparer), descComparer);
        }

        private void SortAlgorithmTest(ISortAlgorithm<T> sortAlgorithm, Func<T, T, bool> compare)
        {
            var list = getItems(100);
            sortAlgorithm.Sort(list);
            AssertIsListSorted(list, compare);
        }

        private void SortDescendingAlgorithmTest(ISortAlgorithm<T> sortAlgorithm, Func<T, T, bool> compare)
        {
            var list = getItems(100);
            sortAlgorithm.SortDescending(list);
            AssertIsListSorted(list, compare);
        }

        private void AssertIsListSorted(List<T> list, Func<T, T, bool> compare)
        {
            for (int i = 0; i < list.Count - 1; i++)
            {
                if (compare(list[i], list[i + 1]))
                {
                    Assert.Fail("not sorted");
                }
            }
        }
    }
}