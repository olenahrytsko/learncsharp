﻿using LearnCSharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LearnCSharpTest.Sort
{
    [TestClass]
    public class IntSortTest : SortingTestBase<int>
    {
        public IntSortTest() : base(ListHelper.CreateRandomList, (x, y) => x > y, (x, y) => x < y)
        {
        }
    }
}

