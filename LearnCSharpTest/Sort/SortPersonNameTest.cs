﻿using System;
using LearnCSharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LearnCSharpTest.Sort
{
    [TestClass]
   public  class SortPersonNameTest : SortingTestBase<PersonName>
    {
        public SortPersonNameTest() : base(ListHelper.CreatePersonList,
            (x, y) => String.CompareOrdinal(x.Name, y.Name) > 0, (x, y) => String.CompareOrdinal(x.Name, y.Name) < 0)
        {
        }
    }
}
