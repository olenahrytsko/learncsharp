﻿using System;
using System.Collections.Generic;
using System.Linq;
using LearnCSharp.DataStructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LearnCSharpTest.DataStructure
{
    [TestClass]
    public class MyTreeTest
    {
        private int count = 9;

        [TestMethod]
        public void MyTreeAddTest()
        {
            var tree = new MyTree<int, int>();
            tree.Add(8, 8);
            tree.Add(10, 10);
            tree.Add(2, 2);
            tree.Add(0, 0);
            tree.Add(9, 9);
            tree.Add(3, 3);
            tree.Add(11, 11);
            Assert.AreEqual(7, tree.Count);
            var infix = tree.TraverseInfix().Select(x => x.Key).ToList();
            var expected = new List<int> {0, 2, 3, 8, 9, 10, 11};
            CollectionAssert.AreEqual(expected, infix);
        }

       
        [TestMethod]
        public void MyTreePostFixTest()
        {
            var tree = new MyTree<int, int>();
            tree.Add(8, 8);
            tree.Add(10, 10);
            tree.Add(2, 2);
            tree.Add(0, 0);
            tree.Add(9, 9);
            tree.Add(3, 3);
            tree.Add(11, 11);
            Assert.AreEqual(7, tree.Count);
            var postfix = tree.TraversePostFix().Select(x => x.Key).ToList();
            var expected = new List<int> { 0, 3, 2,  9, 11, 10, 8};
            CollectionAssert.AreEqual(expected, postfix);
        }

        [TestMethod]
        public void MyTreeRemoveWithoutChildTest()
        {
            var tree = CreateTreeNode();
            tree.Remove(3);
            Assert.AreEqual(11, tree.Count);
            var infix = tree.TraverseInfix().Select(x => x.Key).ToList();
            var expected = new List<int> {5, 7, 8, 10, 18, 20, 21,23,24,25,30};
            CollectionAssert.AreEqual(expected, infix);
        }

        [TestMethod]
        public void MyTreeRemoveWithTwoChildTest()
        {
            var tree = CreateTreeNode();
            tree.Remove(20);
            Assert.AreEqual(11, tree.Count);
            var infix = tree.TraverseInfix().Select(x => x.Key).ToList();
            var expected = new List<int> { 3, 5, 7,8, 10, 18, 21, 23, 24, 25, 30 };
            CollectionAssert.AreEqual(expected, infix);
        }

        [TestMethod]
        public void MyTreeRemoveWithOneLeftChildTest()
        {
            var tree = CreateTreeNode();
            tree.Remove(8);
            Assert.AreEqual(11, tree.Count);
            var infix = tree.TraverseInfix().Select(x => x.Key).ToList();
            var expected = new List<int> {3, 5, 7, 10, 18, 20, 21, 23, 24, 25, 30};
            CollectionAssert.AreEqual(expected, infix);
        }

        [TestMethod]
        public void MyTreeRemoveRootWithOneRigthChildTest()
        {
            var tree = new MyTree<int, int>();
            tree.Add(10, 10);
            tree.Add(20, 20);
            tree.Add(18, 18);
            tree.Add(25, 25);
            tree.Add(23, 23);
            tree.Add(21, 21);
            tree.Add(24, 24);
            tree.Add(30, 30);
            tree.Remove(10);
            Assert.AreEqual(7, tree.Count);
            var infix = tree.TraverseInfix().Select(x => x.Key).ToList();
            var expected = new List<int> { 18, 20, 21, 23, 24, 25, 30 };
            CollectionAssert.AreEqual(expected, infix);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void MyTreeFindNodeTest()
        {
            var tree = new MyTree<int, int>();
            tree.Add(8, 8);
            tree.Add(3, 3);
            tree.Add(10, 10);
            tree.Add(11, 11);
            tree.Add(1, 1);
            tree.Add(2, 2);
            var n = tree.Find(0);
           //Assert.AreEqual(1, n.Key);
        }

        public MyTree<int,int> CreateTreeNode()
        {
            var tree = new MyTree<int, int>();
            tree.Add(10, 10);
            tree.Add(5, 5);
            tree.Add(3, 3);
            tree.Add(8, 8);
            tree.Add(7, 7);
            tree.Add(20, 20);
            tree.Add(18, 18);
            tree.Add(25, 25);
            tree.Add(23, 23);
            tree.Add(21, 21);
            tree.Add(24, 24);
            tree.Add(30, 30);
            return tree;
        }
    }
}