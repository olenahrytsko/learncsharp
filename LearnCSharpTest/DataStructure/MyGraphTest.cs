﻿using System.Collections.Generic;
using System.Linq;
using LearnCSharp.DataStructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LearnCSharpTest.DataStructure
{
    [TestClass]
    public class MyGraphTest
    {
        
        [TestMethod]
        public void BFSMethodTest()
        {
            var graph = CreateGraph();
            var start = graph.Nodes[0];
            var bfs = graph.BFS(start).Select(x => x.Key).ToList();
            var expected = new List<int> {0, 1, 4, 2, 3};
            CollectionAssert.AreEqual(expected, bfs);
        }

        [TestMethod]
        public void DFSMethodTest()
        {
            var graph = CreateGraph();
            var start = graph.Nodes[0];
            var dfs = graph.DFS(start).Select(x => x.Key).ToList();
            var expected = new List<int> { 0, 4, 1, 3, 2 };
            CollectionAssert.AreEqual(expected, dfs);
        }

        public MyGraph<int> CreateGraph()
        {
            var graph = new MyGraph<int>();
            var node0 = new GraphNode<int>(0);
            var node1 = new GraphNode<int>(1);
            var node2 = new GraphNode<int>(2);
            var node3 = new GraphNode<int>(3);
            var node4 = new GraphNode<int>(4);
            node0.List.Add(node1);
            node0.List.Add(node4);
            node1.List.Add(node0);
            node1.List.Add(node4);
            node1.List.Add(node2);
            node1.List.Add(node3);
            node2.List.Add(node1);
            node2.List.Add(node3);
            node3.List.Add(node1);
            node3.List.Add(node4);
            node3.List.Add(node2);
            node4.List.Add(node3);
            node4.List.Add(node0);
            node4.List.Add(node1);
            graph.Add(node0);
            graph.Add(node1);
            graph.Add(node2);
            graph.Add(node3);
            graph.Add(node4);
            return graph;
        }
    }
}