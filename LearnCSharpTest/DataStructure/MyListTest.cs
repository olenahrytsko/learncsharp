﻿using LearnCSharp.DataStructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace LearnCSharpTest.DataStructure
{
    [TestClass]
    public class MyListTest
    {
        [TestMethod]
        public void MyListAddTest()
        {
            int count = 9;
            var list = CreateList(count);
            Assert.AreEqual(count, list.Count);
            for (int i = 0; i < list.Count; i++)
            {
                Assert.AreEqual(i, list[i]);
            }
        }

        [TestMethod]
        public void MyListClearTest()
        {
            int count = 5;
            var list = CreateList(count);
            list.Clear();
            Assert.AreEqual(0, list.Count);
        }
        

      [TestMethod]
        public void MyListContainsTest()
        {
            int count = 5;
            var list = CreateList(count);
            list.Contains(3);
            Assert.AreEqual(true, list.Contains(3));
            list.IndexOf(6);
            Assert.AreEqual(false, list.Contains(6));

        }

        [TestMethod]
        public void MyListIndexOfTest()
        {
            int count = 5;
            var list = CreateList(count);
            list.IndexOf(3);
            Assert.AreEqual(3, list.IndexOf(3));
            list.IndexOf(6);
            Assert.AreEqual(-1, list.IndexOf(6));

        }

        [TestMethod]
        public void MyListAddInPositionTest()
        {
            int count = 5;
            var list = CreateList(count);
            list.Insert(1, 2);
             Assert.AreEqual(2,list[1]);
                Assert.AreEqual(count +1, list.Count);
            for (int i = 2; i < list.Count; i++)
            {
                Assert.AreEqual(i-1, list[i]);
            }
        }

        [TestMethod]
        public void MyListRemoveAtTest()
        {
            int count = 5;
            int index = 1;
            var list = CreateList(count);
            list.RemoveAt(index);
            Assert.AreEqual(count - 1, list.Count);
            for (int i = 0; i < index; i++)
            {
                Assert.AreEqual(i, list[i]);
            }

            for (int i = index + 1; i < list.Count; i++)
            {
                Assert.AreEqual(i + 1, list[i]);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void MyListTestIndex()
        {
            var list = CreateList(10);
            var elem = list[11];
        }

        private static MyList<int> CreateList(int count)
        {
            var list = new MyList<int>();
            for (int i = 0; i < count; i++)
                list.Add(i);
            return list;
        }
    }
}