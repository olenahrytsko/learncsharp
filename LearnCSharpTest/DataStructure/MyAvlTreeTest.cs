﻿using System.Collections.Generic;
using System.Linq;
using LearnCSharp.DataStructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LearnCSharpTest.DataStructure
{
    [TestClass]
    public class MyAvlTreeTest
    {
        [TestMethod]
        public void MyAvlTreeAddTest()
        {
            var avlTree = new MyAvlTree<int, int>();
            avlTree.Add(10, 10);
            avlTree.Add(20, 20);
            avlTree.Add(30, 30);
            avlTree.Add(40, 40);
            avlTree.Add(50, 50);
            avlTree.Add(25, 25);
            //avlTree.Add(16, 16);
            //avlTree.Add(15, 15);
            //Assert.AreEqual(8, avlTree.Count);
            var prefix = avlTree.TraversePrefix().Select(x => x.Key).ToList();
            // var expected = new List<int> {30, 20, 15, 10, 16, 25, 40, 50};
            var expected = new List<int> {30, 20, 10, 25, 40, 50};
            CollectionAssert.AreEqual(expected, prefix);
        }

        [TestMethod]
        public void MyAvlTreeAddParentTest()
        {
            var avlTree = new MyAvlTree<int, int>();
            avlTree.Add(10, 10);
            avlTree.Add(20, 20);
            avlTree.Add(30, 30);
            avlTree.Add(40, 40);
            avlTree.Add(50, 50);
            avlTree.Add(25, 25);
            Assert.AreEqual(6, avlTree.Count);
            var prefix = avlTree.TraversePrefix().Select(x => x.Parent?.Key).ToList();
            var expected = new List<int?> { null, 30, 20, 20, 30, 40 };
            CollectionAssert.AreEqual(expected, prefix);
        }


        [TestMethod]
        public void MyAvlTreeRemoveTest()
        {
            var avlTree = CreateAvlTree();
            avlTree.Remove(10);
            avlTree.Remove(0);
            //Assert.AreEqual(8, avlTree.Count);
            Assert.AreEqual(7, avlTree.Count);
            var prefix = avlTree.TraversePrefix().Select(x => x.Key).ToList();
            //  var expected = new List<int> {1, 0, -1, 9, 5, 2, 6, 11};
            var expected = new List<int> { 5,1,-1,2,9,6,11};
            CollectionAssert.AreEqual(expected, prefix);
        }

        //public MyTree<int, int> CreateAvlTree()
        //{
        //    var avlTree = new MyAvlTree<int, int>();
        //    avlTree.Add(10, 10);
        //    avlTree.Add(20, 20);
        //    avlTree.Add(30, 30);
        //    avlTree.Add(40, 40);
        //    avlTree.Add(50, 50);
        //    avlTree.Add(25, 25);
        //    return avlTree;
        //}
        public MyTree<int, int> CreateAvlTree()
        {
            var avlTree = new MyAvlTree<int, int>();
            avlTree.Add(9, 9);
            avlTree.Add(5, 5);
            avlTree.Add(10, 10);
            avlTree.Add(0, 0);
            avlTree.Add(6, 6);
            avlTree.Add(11, 11);
            avlTree.Add(-1, -1);
            avlTree.Add(1, 1);
            avlTree.Add(2, 2);
            return avlTree;
        }
    }
}