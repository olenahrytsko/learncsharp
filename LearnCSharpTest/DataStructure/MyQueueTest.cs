﻿using LearnCSharp.DataStructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LearnCSharpTest.DataStructure
{
    [TestClass]
    public class MyQueueTest
    {
        private int count = 10;

        [TestMethod]
        public void MyQueueAddTest()
        {
            var queue = CreateMyQueueTest();
            queue.Add(20);
            queue.Add(50);
            Assert.AreEqual(count+2,queue.Count);
         
        }
        [TestMethod]
        public void MyQueuePopTest()
        {
            var queue = CreateMyQueueTest();
            Assert.AreEqual(0, queue.Pop());
            Assert.AreEqual(1, queue.Pop());
            Assert.AreEqual(8, queue.Count);
        }

        [TestMethod]
        public void MyQueuePeekTest()
        {
            var queue = CreateMyQueueTest();
            queue.Add(12);
            Assert.AreEqual(0, queue.Peek());
        }


        public MyQueue<int> CreateMyQueueTest()
        {
            var queue = new MyQueue<int>();
            for (int i = 0; i < count; i++)
            {
                queue.Add(i);
            }

            return queue;

        }
    }
}