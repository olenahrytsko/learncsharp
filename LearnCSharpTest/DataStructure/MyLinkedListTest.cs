﻿using System.Linq;
using LearnCSharp.DataStructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LearnCSharpTest.DataStructure
{
    [TestClass]
    public  class MyLinkedListTest
    {
        private int count = 10;

        [TestMethod]
        public void MyLinkedListAddTest()
        {
            var linkedList = CreateMyLinkedListTest();
            var listed = linkedList.GetAll().ToList();
        }
        [TestMethod]
        public void MyLinkedListSetItemTest()
        {
            var linkedList = CreateMyLinkedListTest();
            linkedList[1] = 200;
            Assert.AreEqual(200, linkedList[1]);
        }

        [TestMethod]
        public void MyLinkedListInsertFirstTest()
        {
            var linkedList = CreateMyLinkedListTest();
            linkedList.Insert(0, 20);
            Assert.AreEqual(20, linkedList[0]);
            Assert.AreEqual(9, linkedList[1]);
            Assert.AreEqual(count + 1, linkedList.Count);
        }
        [TestMethod]
        public void MyLinkedListInsertMiddleTest()
        {
            var linkedList = CreateMyLinkedListTest();
            linkedList.Insert(5, 20);
            Assert.AreEqual(20, linkedList[5]);
            Assert.AreEqual(4, linkedList[6]);
            Assert.AreEqual(count + 1, linkedList.Count);
        }

        [TestMethod]
        public void MyLinkedListInsertLastTest()
        {
            var linkedList = CreateMyLinkedListTest();
            linkedList.Insert(9, 20);
            Assert.AreEqual(20, linkedList[9]);
            Assert.AreEqual(0, linkedList[10]);
            Assert.AreEqual(count + 1, linkedList.Count);
        }

        [TestMethod]
        public void MyLinkedListRemoveFirstTest()
        {
            var linkedList = CreateMyLinkedListTest();
            var b = linkedList.Remove(9);
            Assert.IsTrue(b);
            Assert.AreEqual(8, linkedList[0]);
            Assert.AreEqual(count - 1, linkedList.Count);
        }

        [TestMethod]
        public void MyLinkedListRemoveNonExistentTest()
        {
            var linkedList = CreateMyLinkedListTest();
            var b = linkedList.Remove(777);
            Assert.IsFalse(b);
            Assert.AreEqual(count, linkedList.Count);
        }

        [TestMethod]
        public void MyLinkedListRemoveMiddleTest()
        {
            var linkedList = CreateMyLinkedListTest();
            linkedList.Remove(5);
            Assert.AreEqual(4,linkedList[4]);
            Assert.AreEqual(count-1, linkedList.Count);
        }
        [TestMethod]
        public void MyLinkedListRemoveLastTest()
        {
            var linkedList = CreateMyLinkedListTest();
            linkedList.Remove(0);
            Assert.AreEqual(count - 1, linkedList.Count);
            Assert.AreEqual(1, linkedList[8]);
        }

        [TestMethod]
        public void MyLinkedListIndexOfFirstTest()
        {
            var linkedList = CreateMyLinkedListTest();
            Assert.AreEqual(count - 1, linkedList.IndexOf(0));
            Assert.AreEqual(count - 1, linkedList.IndexOf(0));
        }

        [TestMethod]
        public void MyLinkedListIndexOfMiddleTest()
        {
            var linkedList = CreateMyLinkedListTest();
            Assert.AreEqual(5, linkedList.IndexOf(4));
        }

        [TestMethod]
        public void MyLinkedListIndexOfLastTest()
        {
            var linkedList = CreateMyLinkedListTest();
             linkedList.IndexOf(9);
            Assert.AreEqual(0, linkedList.IndexOf(count - 1));
        }

        [TestMethod]
        public void MyLinkedListRemoveAtFirstTest()
        {
            var linkedList = CreateMyLinkedListTest();
            linkedList.RemoveAt(0);
            Assert.AreEqual(8, linkedList[0]);
        }

        [TestMethod]
        public void MyLinkedListRemoveAtMiddleTest()
        {
            var linkedList = CreateMyLinkedListTest();
            linkedList.RemoveAt(5);
            Assert.AreEqual(3, linkedList[5]);
        }

        [TestMethod]
        public void MyLinkedListRemoveAtLastTest()
        {
            var linkedList = CreateMyLinkedListTest();
            linkedList.RemoveAt(linkedList.Count - 1);
            Assert.AreEqual(9, linkedList.Count);
            Assert.AreEqual(1, linkedList[linkedList.Count - 1]);
        }

        [TestMethod]
        public void MyLinkedListIndexTest()
        {
            var linkedList = CreateMyLinkedListTest();
            for (int i = 0; i < count; i++)
            {
                Assert.AreEqual(i, linkedList[count-1 - i]);
            }
            for (int i = 0; i < count; i++)
            {
                linkedList[i] = count-1 - i;
            }
            for (int i = 0; i < count; i++)
            {
                Assert.AreEqual(count -1- i, linkedList[i]);
            }
        }

        public MyLinkedList<int> CreateMyLinkedListTest()
        {
            var linkedList = new MyLinkedList<int>();
         
            for (int i = 0; i < count; i++)
            {
                linkedList.Add(i);

            }
            return linkedList;
        }
    }
}
