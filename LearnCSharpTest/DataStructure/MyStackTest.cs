﻿using LearnCSharp.DataStructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LearnCSharpTest.DataStructure
{
    [TestClass]
    public class MyStackTest
    {
        private int count = 10;

        [TestMethod]
        public void MyLinkedListAddTest()
        {
            var stack = CreateMyStackTest();
            stack.Add(10);
            Assert.AreEqual(count + 1, stack.Count);
        }


        [TestMethod]
        public void MyLinkedListPeekTest()
        {
            var stack = CreateMyStackTest();
            stack.Add(10);
            stack.Add(12);
            Assert.AreEqual(12, stack.Peek());
        }

        [TestMethod]
        public void MyLinkedListPopTest()
        {
            var stack = CreateMyStackTest();
            Assert.AreEqual(9, stack.Pop());
            Assert.AreEqual(9, stack.Count);
        }

        public MyStack<int> CreateMyStackTest()
        {
            var stack = new MyStack<int>();
            for (int i = 0; i < count; i++)
            {
                stack.Add(i);
            }
            return stack;
        }
    }
}