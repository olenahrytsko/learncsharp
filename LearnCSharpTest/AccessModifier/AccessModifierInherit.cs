﻿using System;
using LearnCSharp.AccessModifier;

namespace LearnCSharpTest.AccessModifier
{
    public class AccessModifierInherit : AccessModifiers
    {
        public void Call()
        {
            //Console.WriteLine(MyInternalProp);
            Console.WriteLine(MyProtectedInternalProp);
            Console.WriteLine(MyPublicProp);
            //Console.WriteLine(MyPrivateProp);
            Console.WriteLine(MyProtectedProp);
        }
    }
}