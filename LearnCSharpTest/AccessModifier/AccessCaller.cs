﻿using System;
using LearnCSharp.AccessModifier;

namespace LearnCSharpTest.AccessModifier
{
    public class AccessCaller
    {
        public void Call()
        {
            var am = new AccessModifiers();
            //Console.WriteLine(am.MyInternalProp);
            //Console.WriteLine(am.MyProtectedInternalProp);
            Console.WriteLine(am.MyPublicProp);
            //Console.WriteLine(am.MyPrivateProp);
            //Console.WriteLine(am.MyProtectedProp);
        }
    }
}