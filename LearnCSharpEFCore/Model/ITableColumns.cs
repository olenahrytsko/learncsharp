﻿using System.Collections.Generic;

namespace LearnCSharpEFCore.Model
{
    public interface ITableColumns
    {
        string TableName { get; set; }

        List<string> Columns { get; set; }
    }
}