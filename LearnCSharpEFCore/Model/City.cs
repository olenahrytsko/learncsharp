﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using LearnCSharpEFCore.Model;


namespace LearnCSharp.DataStructureTask
{
    public class City : IComparable<City>, ITableColumns
    {
        public string Name { get; set; }
        public int CityId { get; set; }
        public ICollection<PersonCity> PersonCities { get; set; }


        public City(string name)
        {
            Name = name;
        }

        //public City()
        //{
        //}
        public City()
        {
            var columns = new List<string>() {"Name"};
            TableName = "Cities";
            Columns = columns;
        }

        protected bool Equals(City other)
        {
            return string.Equals(Name, other.Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((City) obj);
        }

        public override int GetHashCode()
        {
            return (Name != null ? Name.GetHashCode() : 0);
        }

        public override string ToString()
        {
            return $"{Name}";
        }

        public int CompareTo(City other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return string.Compare(Name, other.Name, StringComparison.Ordinal);
        }
        [NotMapped]
        public string TableName { get; set; }
        [NotMapped]
        public List<string> Columns { get; set; }
    }
}