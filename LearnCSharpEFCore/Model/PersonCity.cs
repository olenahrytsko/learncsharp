﻿using LearnCSharp.DataStructureTask;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LearnCSharpEFCore.Model
{
    public class PersonCity
    {
        [Key] public int PersonCityId { get; set; }
        public int PersonId { get; set; }
        public Person Person { get; set; }
        public int CityId { get; set; }
        public City City { get; set; }
        [NotMapped] public string TableName { get; set; }
        [NotMapped] public List<string> Columns { get; set; }

        public PersonCity()
        {
            var columns = new List<string>() {"PersonId", "CityId"};
            TableName = "PersonCity";
            Columns = columns;
        }

        public override string ToString()
        {
            return $"{PersonId} {CityId} ";
        }
    }
}