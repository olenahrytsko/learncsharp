﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace LearnCSharpEFCore.Model
{
    public class Person:ITableColumns
    {
        public string Name { get; set; }
        public string Nickname { get; set; }
        public string Surname { get; set; }
             [NotMapped]
        public string City { get; set; }
        public string PhoneNumber { get; set; }
        public int PersonId { get; set; }
        public ICollection<PersonCity> PersonCities { get; set; }

        public Person(string name, string surname, string nickname, string city, string phoneNumber)
        {
            Name = name;
            Surname = surname;
            Nickname = nickname;
            City = city;
            PhoneNumber = phoneNumber;
        }

        public Person()
        {
            var columns = new List<string>() { "Name", "Nickname", "Surname", "PhoneNumber" };
            TableName = "Persons";
            Columns = columns;
        }

        private sealed class NameNicknameSurnameEqualityComparer : IEqualityComparer<Person>
        {
            public bool Equals(Person x, Person y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (ReferenceEquals(x, null)) return false;
                if (ReferenceEquals(y, null)) return false;
                if (x.GetType() != y.GetType()) return false;
                return string.Equals(x.Name, y.Name) && string.Equals(x.Nickname, y.Nickname) &&
                       string.Equals(x.Surname, y.Surname);
            }

            public int GetHashCode(Person obj)
            {
                unchecked
                {
                    var hashCode = (obj.Name != null ? obj.Name.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (obj.Nickname != null ? obj.Nickname.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (obj.Surname != null ? obj.Surname.GetHashCode() : 0);
                    return hashCode;
                }
            }
        }

        public static IEqualityComparer<Person> NameNicknameSurnameComparer { get; } =
            new NameNicknameSurnameEqualityComparer();



        protected bool Equals(Person other)
        {
            return string.Equals(Name, other.Name) && string.Equals(Nickname, other.Nickname) &&
                   string.Equals(Surname, other.Surname) && string.Equals(City, other.City) &&
                   string.Equals(PhoneNumber, other.PhoneNumber);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Person) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Nickname != null ? Nickname.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Surname != null ? Surname.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (City != null ? City.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (PhoneNumber != null ? PhoneNumber.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ PersonId;
                return hashCode;
            }
        }

        public override string ToString()
        {
            return $"{Name} {Surname} {Nickname} {City} {PhoneNumber}";
        }
        [NotMapped]
        public string TableName { get; set; }
        [NotMapped]
        public List<string> Columns { get; set; }
    }
}