﻿using LearnCSharp.DataStructureTask;
using LearnCSharpEFCore.Model;
using Microsoft.EntityFrameworkCore;


namespace LearnCSharpEFCore
{
    public class ApplicationContext : DbContext
    {
        private readonly string connectionString;

        public DbSet<Person> Persons { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<PersonCity> PersonsCities { get; set; }
        public DbSet<User> Users { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }

        public ApplicationContext(string connectionString)
        {
            this.connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<PersonCity>()
            //    .HasKey(bc => new {bc.PersonId, bc.CityId});
            modelBuilder.Entity<PersonCity>()
                .HasOne(bc => bc.Person)
                .WithMany(b => b.PersonCities)
                .HasForeignKey(bc => bc.PersonId);
            modelBuilder.Entity<PersonCity>()
                .HasOne(bc => bc.City)
                .WithMany(c => c.PersonCities)
                .HasForeignKey(bc => bc.CityId);
        }
    }
}
