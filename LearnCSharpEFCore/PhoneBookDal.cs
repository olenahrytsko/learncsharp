﻿using LearnCSharp.DataStructureTask;
using LearnCSharpEFCore.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace LearnCSharpEFCore
{
    public class PhoneBookDal
    {
        private readonly ApplicationContext db;

        public PhoneBookDal(ApplicationContext db)
        {
            this.db = db;
        }

        public void Add(Person person)
        {
            int cityId = GetCityId(person);
            db.Persons.Add(person);
            db.SaveChanges();
            var pc = CreateCityPerson(person, cityId);
            db.PersonsCities.Add(pc);
            db.SaveChanges();
        }

        private int GetCityId(Person person)
        {
            var cityId = db.Cities.FirstOrDefault(c => c.Name == person.City)?.CityId;
            return cityId.HasValue ? cityId.Value : Add(new City(person.City));
        }

        private PersonCity CreateCityPerson(Person person, int cityId)
        {
            return new PersonCity() {CityId = cityId, PersonId = person.PersonId};
        }

        public int Add(City city)
        {
            var cityId = db.Cities.FirstOrDefault(c => c.Name == city.Name)?.CityId;
            if (cityId == null)
            {
                db.Cities.Add(city);
                db.SaveChanges();
                return city.CityId;
            }

            return city.CityId;
        }

        public void Delete(int id)
        {
            db.Remove(db.Persons.Single(a => a.PersonId == id));
            db.Remove(db.PersonsCities.Single(a => a.PersonId == id));
            db.SaveChanges();
        }

        public void UpDate(Person person)
        {
            var persons = db.Persons.FirstOrDefault(a => a.PersonId == person.PersonId);
            if (persons != null)
            {
                persons.PersonId = person.PersonId;
                persons.Name = person.Name;
                persons.Nickname = person.Nickname;
                persons.Surname = person.Surname;
                persons.PhoneNumber = person.PhoneNumber;
                db.Persons.Update(persons);
                db.SaveChanges();
            }
        }

        public DbSet<Person> GetAll()
        {
            return db.Persons;
        }

        public IQueryable<Person> Find(string name)
        {
            return db.Persons.Where(a => a.Name == name);
        }

        public DataTable GetCityTable(string path)
        {
            var c = new City();
            return MakeTable(path, c.TableName, c.Columns);
        }

        public DataTable GetPersonTable(string path)
        {
            var c = new Person();
            return MakeTable(path, c.TableName, c.Columns);
        }

        public DataTable GetPersonCityTable(List<PersonCity> personCities)
        {
            var c = new PersonCity();
            return MakeTable(personCities, c.TableName, c.Columns);
        }

        public DataTable MakeTable(List<PersonCity> personCities, string tableName, List<string> columns)
        {
            var dt = new DataTable();
            for (var i = 0; i < columns.Count; i++)
            {
                dt.Columns.Add(new DataColumn(columns[i]));
            }

            foreach (var item in personCities)
            {
                var row = dt.NewRow();
                row.ItemArray = new object[] {item.PersonId, item.CityId};
                dt.Rows.Add(row);
            }

            return dt;
        }

        public DataTable MakeTable(string path, string tableName, List<string> columns)
        {
            var dt = new DataTable();
            for (var i = 0; i < columns.Count; i++)
            {
                dt.Columns.Add(new DataColumn(columns[i]));
            }

            using (var sr = new StreamReader(path))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    var data = line.Split('|');
                    var row = dt.NewRow();
                    row.ItemArray = data;
                    dt.Rows.Add(row);
                }
            }

            return dt;
        }

        public void BulkInsert(string connectionString, string tableName, DataTable dt)
        {
            using (var cn = new SqlConnection(connectionString))
            {
                cn.Open();
                using (var copy = new SqlBulkCopy(cn))
                {
                    SetColumnMappings(dt, copy);
                    copy.DestinationTableName = tableName;
                    copy.WriteToServer(dt);
                }
            }
        }

        private static void SetColumnMappings(DataTable dataTable, SqlBulkCopy bulkCopy)
        {
            foreach (var column in dataTable.Columns)
            {
                bulkCopy.ColumnMappings.Add(column.ToString(), column.ToString());
            }
        }

        public List<PersonCity> GetPersonCity()
        {
            var pc = new List<PersonCity>();
            var dbCities = db.Cities.Select(n => new {n.Name, n.CityId});
            var cityDic = new SortedDictionary<string, int>(dbCities.ToDictionary(c => c.Name, c => c.CityId));
            var person = db.Persons.Select(p => new {p.PersonId, p.City})
                .ToList();
            foreach (var item in person)
            {
                if (cityDic.TryGetValue(item.City, out int value))
                {
                    var perCity = new PersonCity() {CityId = value, PersonId = item.PersonId};
                    pc.Add(perCity);
                }
                else
                {
                    Console.WriteLine($"CityName-{item.City} not present!");
                }
            }

            return pc;
        }
    }
}
