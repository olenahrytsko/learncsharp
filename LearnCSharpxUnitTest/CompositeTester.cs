﻿using FluentAssertions;
using Xunit;

namespace LearnCSharp.Patterns.Composite
{
    public class CompositeTester
    {
        [Fact]
        public void ShouldReturnValidPower()
        {
            var turbine1 = new Turbine();
            var turbine2 = new Turbine();
            var turbine3 = new Turbine();
            var park1 = new Park(turbine1, turbine2);
            var park2 = new Park(turbine3);
            IWindTurbine megaPark = new Park(park1, turbine1);
            var power = megaPark.GetPower();
            power.Should().Be(30);
        }
    }
}
