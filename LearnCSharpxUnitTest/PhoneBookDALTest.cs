using System.Data;
using LearnCSharp.DataStructureTask;
using LearnCSharpEFCore;
using LearnCSharpEFCore.Model;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Xunit;

namespace LearnCSharpxUnitTest
{
    public class PhoneBookDalTest
    {
        [Fact]
        public void Add_Test()
        {
            var user1 = new Person() { Name = "Git", City = "dfgjkadfn", Nickname = "gggt" };
            var context = CreateDb();
            var service = new PhoneBookDal(context);
            service.Add(user1);
            Assert.Equal(5, context.Persons.Count());
            Assert.Equal(user1, context.Persons.SingleOrDefault(x => x.Name == "Git"));
        }
        [Fact]
        public void Add_PersonCity_Test()
        {
            var user1 = new Person() { Name = "Git", City = "Odessa", Nickname = "5" };
            var context = CreateDb();
            var service = new PhoneBookDal(context);
            service.Add(user1);
            Assert.Equal(5, context.Persons.Count());
            Assert.Equal(5, context.PersonsCities.Count());
            Assert.Equal(3, context.Cities.Count());
            Assert.Equal(5, context.PersonsCities.SingleOrDefault(p => p.PersonId == 5).PersonId);
            Assert.Equal(user1, context.Persons.SingleOrDefault(x => x.Name == "Git"));
        }
        [Fact]
        public void AddCity_Test()
        {
            var user1 = new City() { Name = "Lviv" };
            var context = CreateDb();
            var service = new PhoneBookDal(context);
            service.Add(user1);
            Assert.Equal(2, context.Cities.Count());
            Assert.Equal(user1, context.Cities.SingleOrDefault(x => x.Name == "Lviv"));
        }

        [Fact]
        public void Delete_Test()
        {
            var context = CreateDb();
            var service = new PhoneBookDal(context);
            Assert.Equal(4, context.Persons.Count());
            service.Delete(2);
            Assert.Equal(3, context.Persons.Count());
            Assert.Null(context.Persons.SingleOrDefault(x => x.Name == "Nikita"));
        }

        [Fact]
        public void Update_Test()
        {
            var user1 = new Person() { PersonId = 2, Name = "Nikita", City = "Kiev", Nickname = "dgj" };
            var context = CreateDb();
            var service = new PhoneBookDal(context);
            service.UpDate(user1);
            Assert.Equal(4, context.Persons.Count());
            Assert.Equal(user1, context.Persons.SingleOrDefault(x => x.Nickname == "dgj"));
        }

        [Fact]
        public void Find_Test()
        {
            var context = CreateDb();
            var service = new PhoneBookDal(context);
            var list = service.Find("Olena").Select(a => new { Id = a.PersonId, a.Name }).ToList();
            Assert.Equal(2, list.Count);
        }

        [Fact]
        public void Temp_Test()
        {
            var path = @".\City.txt";
            var context = CreateDb();
            var service = new PhoneBookDal(context);
            //var dt = service.MakeTable(path);
        }

        private static ApplicationContext CreateDb()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase("Test")
                .Options;
            var user1 = new Person() { Name = "Olena", City = "Lviv", Nickname = "1" };
            var user2 = new Person() { Name = "Nikita", City = "Kiev", Nickname = "2" };
            var user3 = new Person() { Name = "Olena", City = "Kiev", Nickname = "3" };
            var user4 = new Person() { Name = "Nik", City = "Lviv", Nickname = "4" };
            var city1 = new City() { Name = "Lviv" };
            var city2 = new City() { Name = "Kiev" };
            var context = new ApplicationContext(options);
            var service = new PhoneBookDal(context);
            service.Add(user1);
            service.Add(user2);
            service.Add(user3);
            service.Add(user4);
            service.Add(city1);
            service.Add(city2);
            return context;
        }
    }
}
