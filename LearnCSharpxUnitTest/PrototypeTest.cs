﻿using FluentAssertions;
using LearnCSharp.Patterns.Prototype;
using System;
using Xunit;

namespace LearnCSharpxUnitTest
{
    public class PrototypeTest
    {
        [Fact]
        public void ShouldReturnValidPrototype()
        {
            var person = new PersonPrototype()
            {
                Age = 34,
                BirthDate = DateTime.UtcNow,
                Name = "Olena",
                Address = new Address()
                {
                    City = "Lviv",
                    Country = "UK"
                }
            };
            var shallowCopy = person.ShallowCopy();
            var deepCopy = person.DeepCopy();
            shallowCopy.Address.Should().Be(person.Address);
            deepCopy.Address.Should().NotBe(person.Address);
        }
    }
}