﻿using System.Collections.Generic;

namespace LearnCSharp.Inheritance
{
    public class InheritanceRun
    {
        public void Run()
        {
            var animals = CreateAnimals();
            var dogs = CreateDogs();
            foreach (var item in animals)
            {
                item.Feed();
                item.Pet();
                if (item is Dog)
                {
                    var dog = item as Dog;
                    dog.Gav();
                }

                if (item is Cat)
                {
                    var cat = item as Cat;
                    cat.May();
                }
            }
            FeedAnimals(dogs);
        }

        public void FeedAnimals(IEnumerable<Animal> listAnimal)
        {
            foreach (var item in listAnimal)
            {
                item.Feed();
            }
        }

        public List<Animal> CreateAnimals()
        {
            var animal = new List<Animal>();
            animal.Add(new Dog());
            animal.Add(new Cat());
            return animal;
        }
        public List<Dog> CreateDogs()
        {
            var animal = new List<Dog>();
            animal.Add(new Dog());
            animal.Add(new Dog());
            return animal;
        }

    }
}