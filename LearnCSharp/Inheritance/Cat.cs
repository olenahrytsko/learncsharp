﻿using System;

namespace LearnCSharp.Inheritance
{
    public class Cat:Animal
    {
        public override void Feed()
        {
            Console.WriteLine("Cat feed");
        }
        public void May()
        {
            Console.WriteLine("May");
        }
    }
}