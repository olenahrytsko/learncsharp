﻿using System;

namespace LearnCSharp.Inheritance
{
    public class Dog : Animal
    {
        public override void Feed()
        {
            base.Feed();
            Console.WriteLine("Dog feed");
        }

        public override sealed void Pet()
        {
            Console.WriteLine("Dog Pet");
        }

        public void Gav()
        {
            Console.WriteLine("Gav");
        }
    }
}