﻿using System;

namespace LearnCSharp.Inheritance
{
    public class PersonEquals
    {
        public bool Person1Equals()
        {
            var p1=new Person1("Olena",30);
            var p2 = new Person1("Olena", 30);
           return p1.Equals(p2);
        }
        public bool Person2Equals()
        {
            var p1 = new Person2("Olena", 30);
            var p2 = new Person2("Olena", 30);
            return p1.Equals(p2);
        }

        public void Run()
        {
           Console.WriteLine(Person1Equals().ToString());
            Console.WriteLine(Person2Equals().ToString());
        }
}
}