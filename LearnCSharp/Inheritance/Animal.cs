﻿using System;

namespace LearnCSharp.Inheritance
{
    public abstract class Animal
    {
        private readonly string Name;
        private readonly int Age;
 
        public virtual  void Feed()
        {
            Console.WriteLine("Feed");
        }
        public virtual void Pet()
        {
            Console.WriteLine("Pet");
        }
    }
}