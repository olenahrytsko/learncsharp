﻿namespace LearnCSharp.Inheritance
{
    public struct Person1
    {
        private readonly string Name;
        private readonly int Age;

        public Person1(string name, int age)
        {
            Name = name;
            Age = age;
        }
    }
}