﻿namespace LearnCSharp.Delegate
{
    public class RunAction
    {
        public void Run()
        {
            var actionMessage = new ActionMessage("Hello");
        }

        public void LogRun()
        {
            var actionMessage = new ActionMessage("Hello to me ", "LogActionMessage.txt");
        }
    }
}