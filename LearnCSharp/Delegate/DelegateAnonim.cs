﻿using System;

namespace LearnCSharp.Delegate
{
    public class DelegateAnonim
    {
        delegate int Operation(int x, int y);
        public  void Run()
        {
            int z = 8;
            Operation operation = delegate (int x, int y)
            {
                return x + y + z;
            };
            int d = operation(4, 5);
            Console.WriteLine(d);       // 17
        }
    }
}