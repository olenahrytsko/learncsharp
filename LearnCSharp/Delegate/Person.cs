﻿using System;

namespace LearnCSharp.Delegate
{
    public class Person
    {
        public BankAccount Account { get; set; }
        public string Name { get; set; }

        public Person(string name, BankAccount account)
        {
            Account = account;
            Name = name;
            Account.Added += OnAccountAlert;
            Account.Withdrawn += OnAccountAlert;
        }

        private void OnAccountAlert(string message)
        {
            Console.WriteLine(message);
        }

        public override string ToString()
        {
            return $"{Name}";
        }
    }
}