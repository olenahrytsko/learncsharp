﻿using System;
using System.Collections.Generic;
using System.IO;

namespace LearnCSharp.Delegate
{
    public class ActionMessage
    {
        private readonly string Message;

        public ActionMessage(string message)
        {
            Message = message;
            Action<string> actionTarget=new Action<string>(DisplayMessage);
            actionTarget(Message);
        }
        public ActionMessage(string message,string path)
        {
            Action<string,string> actionTarget = new Action<string,string>(LogFile);
            actionTarget(message, path);
        }

        private void DisplayMessage(string message)
        {
            Console.WriteLine(message);
        }

        private void LogFile(string message, string path)
        {
            var tw = new StreamWriter(path,true);
            try
            {
                tw.WriteLine(message);
            }
            catch (Exception e)
            {
                Console.WriteLine("file not found");
            }
            finally
            {
                tw.Dispose();
            }
        }
    }
}