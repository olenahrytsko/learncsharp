﻿using Person = LearnCSharp.Delegate.Person;
namespace LearnCSharp.Delegate
{
    public class RunBankAccount
    {
        public RunBankAccount()
        {
            var account = new BankAccount(200);
            var run = new Person("Olena", account);
            run.Account.Withdraw(300);
        }
    }
}