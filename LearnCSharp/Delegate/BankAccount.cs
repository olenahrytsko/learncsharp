﻿using System;

namespace LearnCSharp.Delegate
{
    public class BankAccount
    {
        public delegate void LowBalanceAlert(string message);

        public event LowBalanceAlert Withdrawn;
        public event LowBalanceAlert Added;
        private int balance;

        public BankAccount(int sum)
        {
            balance = sum;
        }

        public void Withdraw(int sum)
        {
            if (balance >= sum)
            {
                balance -= sum;
                if (balance <= 10)
                {
                    if (Withdrawn != null)
                        Withdrawn($"Balance {balance} lower 10");
                    Add(100);

                }
            }
            else
            {
                if (Withdrawn != null)
                    Withdrawn("Not enough money");
            }
        }

        public void Add(int sum)
        {
            balance += sum;

            if (Added != null)
                Added($"Balance  increased by {sum}");
        }
    }
}