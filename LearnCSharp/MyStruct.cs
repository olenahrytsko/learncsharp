﻿using System;

namespace LearnCSharp
{
    public struct MyStruct 
    {
        public int X;
        public int Y;

        public MyStruct(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int Add(int a, int b)
        {
            return a + b;
        }
    }
}