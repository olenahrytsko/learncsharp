﻿using System;
using System.Collections.Generic;

namespace LearnCSharp.Sort
{
    public class InsertionSort<T> : ISortAlgorithm<T>
    {
        private readonly Func<T, T, bool> compare;

        public InsertionSort(Func<T, T, bool> compare)
        {
            this.compare = compare;
        }

        public void Sort(List<T> list)
        {
            SortInternal(list, compare);
        }

        public void SortDescending(List<T> list)
        {
            SortInternal(list, (x, y) => !compare(x,y));
        }

        private void SortInternal(List<T> list, Func<T, T, bool> compareInternal)
        {
            for (int i = 1; i < list.Count; i++)
            {
                var pivot = list[i];
                int j = i - 1;
                while (j >= 0 && compareInternal(list[j], pivot))
                {
                    list[j + 1] = list[j];
                    j = j - 1;
                }
                list[j + 1] = pivot;
            }
        }

    }
}