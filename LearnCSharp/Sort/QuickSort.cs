﻿using System;
using System.Collections.Generic;

namespace LearnCSharp.Sort
{
    public class QuickSort<T> : ISortAlgorithm<T>
    {
        private readonly Func<T, T, bool> compare;

        public QuickSort(Func<T, T, bool> compare)
        {
            this.compare = compare;
        }

        public void Sort(List<T> list)
        {
            QuickSortAlgorithm(list, 0, list.Count, compare);
        }

        public void SortDescending(List<T> list)
        {
            QuickSortAlgorithm(list, 0, list.Count, (x, y) => !compare(x,y));
        }

        private void QuickSortAlgorithm(List<T> list, int left, int right, Func<T, T, bool> compareInternal)
        {
            if (list.Count <= 1)
                return;
            if (left < right)
            {
                var pivotIdx = SortInternal(list, left, right, compareInternal);
                QuickSortAlgorithm(list, left, pivotIdx - 1, compareInternal);
                QuickSortAlgorithm(list, pivotIdx, right, compareInternal);
            }
        }

        private int SortInternal(List<T> list, int left, int right, Func<T, T, bool> compareInternal)
        {
            int start = left;
            var pivot = list[start];
            left++;
            right--;
            while (true)
            {
                while (left <= right && compareInternal(list[left], pivot))//<=
                    left++;
                while (left <= right && !compareInternal(list[right], pivot))//>
                    right--;
                if (left > right)
                {
                    list[start] = list[left - 1];
                    list[left - 1] = pivot;
                    return left;
                }
                ListHelper.Swap(list, left, right);
            }
        }
    }
}

