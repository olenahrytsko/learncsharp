﻿using System;
using System.Collections.Generic;

namespace LearnCSharp.Sort
{
    public class BubbleSort<T> : ISortAlgorithm<T>
    {
        private readonly Func<T, T, bool> compare;

        public BubbleSort(Func<T, T, bool> compare)
        {
            this.compare = compare;
        }

        public void Sort(List<T> list)
        {
            SortInternal(list, compare);
        }

        public void SortDescending(List<T> list)
        {
            SortInternal(list, (x, y) => !compare(x,y));
        }

        private void SortInternal(List<T> list, Func<T, T, bool> compareInternal)
        {
            for (int i = 0; i < list.Count - 1; i++)
            {
                for (int j = 0; j < list.Count - i - 1; j++)
                {
                    if (compareInternal(list[j], list[j + 1]))
                        ListHelper.Swap(list, j, j + 1);
                }
            }
        }
    }
}
