﻿using System.Collections.Generic;

namespace LearnCSharp.Sort
{
    public interface ISortAlgorithm<T>
    {
        void Sort(List<T> list);
        void SortDescending(List<T> list);
    } 
}
