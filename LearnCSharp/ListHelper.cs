﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LearnCSharp.DataStructureTask;
using LearnCSharpEFCore.Model;

namespace LearnCSharp
{
    public static class ListHelper
    {
        public static void ConsoleOutput<T>(this List<T> list)
        {
            if(list==null || list.Count.Equals(0))
            { Console.WriteLine("Not found");}
            else
            {
                for (int i = 0; i < list.Count; i++)
                {
                    Console.WriteLine(list[i]);
                }
            }
        }
        public static void Swap<T>(IList<T> list, int indexA, int indexB)
        {
            var tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
        }

        public static void Swapref(ref int a, ref int b)
        {
            int tmp;
            tmp = a;
            a = b;
            b = tmp;
        }
        public static void Swapref(ref string a, ref string b)
        {
            string tmp;
            tmp = a;
            a = b;
            b = tmp;
        }
        public static void Swap( int a,  int b)
        {
            int tmp;
            tmp = a;
            a = b;
            b = tmp;
        }
        public static List<int> CreateRandomList(int count)
        {
            var list = new List<int>(count);
            var rand = new Random();
            for (int i = 0; i < count; i++)
            {
                int n = rand.Next(1000);
                list.Add(n);
            }
            return list;
        }

        public static string GenerateName(int len, Random r)
        {
            string[] consonants = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "l", "n", "p", "q", "r", "s", "sh", "zh", "t", "v", "w", "x" };
            string[] vowels = { "a", "e", "i", "o", "u", "ae", "y" };
            string name = "";
            name += consonants[r.Next(consonants.Length)].ToUpper();
            name += vowels[r.Next(vowels.Length)];
            int b = 2; //b tells how many times a new letter has been added. It's 2 right now because the first two letters are already in the name.
            while (b < len)
            {
                name += consonants[r.Next(consonants.Length)];
                b++;
                name += vowels[r.Next(vowels.Length)];
                b++;
            }
            return name;
        }

        public static List<string> GenerateCity(string path)
        {
            var list = new List<string>();
            {
                try
                {
                    using (StreamReader sr = new StreamReader(path))
                    {
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            list.Add(line);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("The file could not be read:");
                    Console.WriteLine(e.Message);
                }
            }
            return list;
        }

        public static List<PersonName> CreatePersonList(int count)
        {
            var list = new List<PersonName>(count);
            var rand = new Random();
            for (int i = 0; i < count; i++)
            {
                int n = rand.Next(100);
                string name = GenerateName(5, rand);
                list.Add(new PersonName { Name = name, Age = n });
            }
            return list;
        }

        public static List<Person> GetData(string path)
        {
            var list = new List<Person>();
            {
                try
                {
                    using (StreamReader sr = new StreamReader(path))
                    {
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            var words = line.Split(new[]{'|'}, StringSplitOptions.RemoveEmptyEntries).Select(w => w.Trim()).ToList();
                            list.Add(new Person
                            {
                                Name = words[0], Surname = words[1], Nickname = words[2], City = words[3],
                                PhoneNumber = words[4]
                            });
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("The file could not be read:");
                    Console.WriteLine(e.Message);
                }
            }
            return list;
        }

        public static void SetData(List<Person> list, string path)
        {
            //if (!File.Exists(path))   need  fix it
            //    File.Create(path);
            if (File.Exists(path))
            {
                using (var tw = new StreamWriter(path, true))
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        var newList = list[i];
                        tw.WriteLine(newList);
                    }
                    tw.Close();
                }
            }
        }

        public static List<Person> CreatePerson(int count)
        {
            var list = new List<Person>(count);
            var rand = new Random();
            var cityList = GenerateCity("City.txt");
            for (int i = 0; i < count; i++)
            {
                var n = rand.Next(100000);
                var k = rand.Next(100000);
                var name = GenerateName(5, rand);
                var surname = GenerateName(5, rand);
                var nickname = GenerateName(4, rand);
                var rInt = rand.Next(0, 60);
                var city = cityList[rInt];
                list.Add(new Person() { Name = name, Surname = '|' + surname + '|', Nickname = nickname, City = '|' + city + '|', PhoneNumber = 1 + "-" + n + "-" + k });
            }
            return list;
        }
    }
}
