﻿using System;

namespace LearnCSharp
{
    public class TryExeption
    {
        public void PropertyExeption()
        {
            try
            {
                int x = 5;
                int y = x / 0;
                Console.WriteLine($"Результат: {y}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Исключение: {ex.Message}");
                Console.WriteLine($"Метод: {ex.TargetSite}");
                Console.WriteLine($"Трассировка стека: {ex.StackTrace}");
                Console.WriteLine($"Назва обєкта або збірки, який викликав помилку: {ex.Source}");
                Console.WriteLine($"Причина помилки: {ex.InnerException}");
            }
            finally
            {
                Console.WriteLine($"Причина помилки:");
            }
        }

        public void MyExeption()
        {
            try
            {
                Console.Write("Type string ");
                string message = Console.ReadLine();
                if (message.Length > 6)
                {
                    throw new Exception("lenght string >6");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message}");
            }
            Console.Read();
        }
    }
}