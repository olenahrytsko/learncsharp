﻿using System;
using System.Collections.Generic;

namespace LearnCSharp.DataStructureTask
{
    public class TimeTable

    {
        public string Name { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }

        public override string ToString()
        {
            return $"{Name} {StartDateTime} {EndDateTime}";
        }
    }
}