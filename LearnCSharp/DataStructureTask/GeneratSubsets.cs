﻿using System;
using System.Collections.Generic;

namespace LearnCSharp.DataStructureTask
{
    public class GeneratSubsets
    {
        public void PrintSubsets()
        {
            var list = new List<string> {"ocean", "beer", "money", "happiness"};

            for (var i = 0; i < (1 << list.Count); i++)
            {
                Console.WriteLine("{");
                // Print current subset 
                for (var j = 0; j < list.Count; j++)
                    // (1<<j) is a number with jth bit 1 
                    // subset number we get which numbers 
                    // are present in the subset and which 
                    // are not 
                    if ((i & (1 << j)) > 0)
                        Console.WriteLine(list[j] + " ");
                Console.WriteLine("}");
            }
        }
    }
}