﻿using System.Collections.Generic;

namespace LearnCSharp.DataStructureTask
{
    public class Supermarket
    {
        private readonly SortedSet<Product> products;

        public Supermarket(IEnumerable<Product> products)
        {
            this.products = new SortedSet<Product>(products);
        }

        public SortedSet<Product> GetProducts(Product lowerPrice, Product upperPrice, int count)
        {
            return products.GetViewBetween(lowerPrice, upperPrice);
        }
    }
}