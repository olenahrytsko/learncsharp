﻿using System;
using System.Collections.Generic;
using System.IO;
//using System.IO;
using System.Linq;
using LearnCSharpEFCore.Model;
using Newtonsoft.Json;

namespace LearnCSharp.DataStructureTask
{
    public class PhoneBook : IPhoneBook
    {
        private SortedDictionary<City, List<Person>> cityDictionary;
        private readonly Dictionary<string, List<Person>> phonesDictionary;

        public PhoneBook(string path)
        {
            var data = ListHelper.GetData(path);
            cityDictionary = CreateSortedCityDictionary(data);
            phonesDictionary = CreateDictionary(data);
        }

        public PhoneBook(List<Person> persons)
        {
            cityDictionary = CreateSortedCityDictionary(persons);
            phonesDictionary = CreateDictionary(persons);
        }

        public SortedDictionary<City, List<Person>> CityDictionary
        {
            get { return cityDictionary;}
            set { cityDictionary = value; }
        }

        private static Dictionary<string, List<Person>> CreateDictionary(List<Person> persons)
        {
            var data = new Dictionary<string, List<Person>>();
            foreach (var person in persons)
            {
                IfKeyIsNull(data, person.Name, person);
                IfKeyIsNull(data, person.Surname, person);
                IfKeyIsNull(data, person.Nickname, person);
            }

            return data;
        }

        private static void IfKeyIsNull(Dictionary<string, List<Person>> data,string name, Person person)
        {         
            if (!string.IsNullOrEmpty(name))
            {
                data.AddSafe(name, new List<Person>());
                var persons = data[name];
                persons.Add(person);
            }
        }

        private static SortedDictionary<City, List<Person>> CreateSortedCityDictionary(List<Person> persons)
        {
            var data = new SortedDictionary<City, List<Person>>();
            foreach (var person in persons)
            {
                var city = new City(person.City);
                if (!data.ContainsKey(city))
                    data.Add(city, new List<Person>());
                data[city].Add(person);
            }

            foreach (var item in data)
            {
                item.Value.Sort(Comparison);
            }
            return data;
        }

        public List<Person> FindPersons(string name)
        {
            phonesDictionary.TryGetValue(name, out var list);
            return list;
        }

        public List<Person> FindPersons(string name, string city)
        {
            var newList = new List<Person>();
            if (phonesDictionary.TryGetValue(name, out var list))
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].City.Equals(city))
                    {
                        var newPerson = list[i];
                        newList.Add(newPerson);
                    }
                }
                return newList;
            }

            return newList;
        }



        //todo
        //public static void DoQueries(string path)
        //{
        //    var list = new List<Person>();
        //    var data = new PhoneBook();
        //    list = data.FindPersons("Heshaeju", "New", "PhoneBook.txt");
        //}

        public void ConsoleOutput()
        {
            foreach (var item in cityDictionary)
            {
                Console.WriteLine(item.Key);
                item.Value.ConsoleOutput();
                Console.WriteLine("-------------");
            }
        }

        private static int Comparison(Person x, Person y)
        {
            return String.Compare(x.Name, y.Name, StringComparison.Ordinal);
        }

        public void SaveToFile(string path)
        {
            var tw = new StreamWriter(path);
            try
            {
                foreach (var item in cityDictionary)
                {
                    tw.WriteLine(item.Key);
                    var list = new List<Person>(item.Value);
                    for (int i = 0; i < list.Count; i++)
                    {
                        tw.WriteLine(list[i]);
                    }
                }
            }
            //catch (Exception e)
            //{

            //}
            finally
            {
                tw.Dispose();
            }

            //using (var tw = new StreamWriter(path))
            //{
            //    foreach (var item in cityDictionary)
            //    {
            //        tw.WriteLine(item.Key);
            //        var list = new List<Person>(item.Value);
            //        for (int i = 0; i < list.Count; i++)
            //        {
            //            tw.WriteLine(list[i]);
            //        }
            //    }
            //}
        }

        public void SaveToJson(string path)
        {
            var json = JsonConvert.SerializeObject(cityDictionary.Take(5), Formatting.Indented);
            File.WriteAllText(path, json);
        }
    }
}