﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LearnCSharp.DataStructureTask
{
    public class ConferenceHall
    {
        private readonly List<TimeTable> conference;
        private readonly SortedSet<TimeTable> startList;
        private readonly SortedSet<TimeTable> endList;

        public ConferenceHall(List<TimeTable> conference)
        {
            this.conference = conference;
            startList = CreateStartDataTime();
            endList = CreateEndDataTime();
        }

        private SortedSet<TimeTable> CreateStartDataTime()
        {
            var start = new SortedSet<TimeTable>(new ByStartTime());
            Add(start);
            return start;
        }

        private SortedSet<TimeTable> CreateEndDataTime()
        {
            var start = new SortedSet<TimeTable>(new ByEndTime());
            Add(start);
            return start;
        }

        private void Add(SortedSet<TimeTable> start)
        {
            foreach (var item in conference)
            {
                start.Add(item);
            }

        }

        public SortedSet<string> GetHall(TimeTable data)
        {
            var hall = new SortedSet<string>(conference.Select(a => a.Name));
            var startListData = GetStartData(data);
            var hall1 = new SortedSet<string>(startListData.Select(a => a.Name));
            var endListData = GetEndData(data);
            var hall2 = new SortedSet<string>(endListData.Select(a => a.Name));
            hall.ExceptWith(hall1);
            hall.ExceptWith(hall2);
            return hall;
        }

        public SortedSet<TimeTable> GetStartData(TimeTable data)
        {
            var lower = new TimeTable() {StartDateTime = data.StartDateTime};
            var upper = new TimeTable() {StartDateTime = data.EndDateTime};
            return startList.GetViewBetween(lower, upper);
        }

        public SortedSet<TimeTable> GetEndData(TimeTable data)
        {
            var lower = new TimeTable() {EndDateTime = data.StartDateTime};
            var upper = new TimeTable() {EndDateTime = data.EndDateTime};
            return endList.GetViewBetween(lower, upper);
        }

        private class ByEndTime : IComparer<TimeTable>
        {
            public int Compare(TimeTable x, TimeTable y)
            {
                return DateTime.Compare(x.EndDateTime, y.EndDateTime);
            }
        }

        private class ByStartTime : IComparer<TimeTable>
        {
            public int Compare(TimeTable x, TimeTable y)
            {
                return DateTime.Compare(x.StartDateTime, y.StartDateTime);
            }

        }
    }
}