﻿using System.Collections.Generic;

namespace LearnCSharp.DataStructureTask
{
    static class Extensions
    {
        public static void AddSafe<TKey,T>(this Dictionary<TKey, T> dictionary,
           TKey key, T value)
        {
            if (!dictionary.ContainsKey(key))
                dictionary.Add(key, value);
        }

    }
}