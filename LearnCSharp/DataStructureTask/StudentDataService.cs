﻿using System;
using System.Collections.Generic;
using System.IO;

namespace LearnCSharp.DataStructureTask
{
    public class StudentDataService
    {
        private static List<Student> GetStudents()
        {
            var list = new List<Student>();
            {
                try
                {
                    using (StreamReader sr = new StreamReader("Students.txt"))
                    {
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            string[] words = line.Split('|');
                            list.Add(new Student { Name = words[0], Surname = words[1], Course = words[2] });
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("The file could not be read:");
                    Console.WriteLine(e.Message);
                }
            }
            return list;
        }

        public static Dictionary<Course, List<Student>> BuildCourse()
        {
            var data = new Dictionary<Course, List<Student>>();
            var students = GetStudents();
            foreach (var student in students)
            {
                var course = new Course(student.Course);
                if (!data.ContainsKey(course))
                    data.Add(course, new List<Student>());
                var list = data[course];
                list.Add(student);
            }

            return data;
        }

        public static void ConsoleOutput(Dictionary<Course, List<Student>> dictionary)
        {
            foreach (var item in dictionary)
            {
                Console.WriteLine(item.Key);
                item.Value.Sort(Comparison);
                ListHelper.ConsoleOutput(item.Value);
                Console.WriteLine("-------------");
            }
        }

        private static int Comparison(Student x, Student y)
        {
            var item = String.Compare(x.Surname, y.Surname, StringComparison.Ordinal);
            if (item == 0)
                return String.Compare(x.Name, y.Name, StringComparison.Ordinal);
            if (item == 1)
                return item;
            if (item == -1)
                return item;

            return item;
        }
    }
}