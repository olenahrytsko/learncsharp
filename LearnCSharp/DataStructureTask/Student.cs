﻿namespace LearnCSharp.DataStructureTask
{
    public class Student
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Course { get; set; }

        public override string ToString()
        {
            return $"{Name} {Surname}";
        }
    }
}