﻿using System;

namespace LearnCSharp.DataStructureTask
{
    public class Product : IComparable<Product>
    {
        public int Barcode { get; set; }
        public string Producer { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }

        protected bool Equals(Product other)
        {
            return Barcode == other.Barcode && Price == other.Price;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Product) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Barcode * 397) ^ Price;
            }
        }

        public override string ToString()
        {
            return $"{Price} {Barcode} ";
        }

        public int CompareTo(Product other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return Price.CompareTo(other.Price);
        }
    }
}