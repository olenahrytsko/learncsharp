﻿namespace LearnCSharp.DataStructureTask
{
    public class Course
    {
        public string Name { get; set; }

        public Course(string name)
        {
            Name = name;
        }

        protected bool Equals(Course other)
        {
            return string.Equals(Name, other.Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Course) obj);
        }

        public override int GetHashCode()
        {
            return (Name != null ? Name.GetHashCode() : 0);
        }
        public override string ToString()
        {
            return $"{Name}";
        }
    }
}