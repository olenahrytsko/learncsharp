﻿using System.Collections.Generic;

namespace LearnCSharp.DataStructureTask
{
    public class BiDictionary<TKey1, TKey2, TValue>
    {
        private readonly Dictionary<CompositeKey<TKey1, TKey2>, List<TValue>>
            dictionary = new Dictionary<CompositeKey<TKey1, TKey2>, List<TValue>>();

        public void AddTriples(TKey1 key1, TKey2 key2, TValue value)
        {
            if (key1 != null && key2 != null)
            {
                var key = new CompositeKey<TKey1, TKey2>() {Key1 = key1, Key2 = key2};
                AddKeyValue(dictionary, key, value);
            }

            if (key1 != null)
            {
                var newKey1 = new CompositeKey<TKey1, TKey2>() {Key1 = key1};
                AddKeyValue(dictionary, newKey1, value);
            }

            if (key2 != null)
            {
                var newKey2 = new CompositeKey<TKey1, TKey2>() {Key2 = key2};
                AddKeyValue(dictionary, newKey2, value);

            }
        }

        private static void AddKeyValue(Dictionary<CompositeKey<TKey1, TKey2>, List<TValue>> data, CompositeKey<TKey1, TKey2> key, TValue value)
        {
            data.AddSafe(key, new List<TValue>());
            var values = data[key];
            values.Add(value);
        }


        public List<TValue> Find(TKey1 key1, TKey2 key2)
        {
            var key = new CompositeKey<TKey1, TKey2>()
            {
                Key1 = key1,
                Key2 = key2
            };
            var a = 5;
            object b = a;
            
            if (dictionary.ContainsKey(key))
                return dictionary[key];
            throw new KeyNotFoundException("Key not found!");
        }
    }
}