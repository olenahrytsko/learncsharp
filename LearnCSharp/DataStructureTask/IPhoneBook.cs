﻿using System.Collections.Generic;
using LearnCSharpEFCore.Model;

namespace LearnCSharp.DataStructureTask
{
    public interface IPhoneBook
    {
        List<Person> FindPersons(string name);
        List<Person> FindPersons(string name, string city);
    }
}