﻿using System.Collections.Generic;

namespace LearnCSharp.DataStructureTask
{
    public class CompositeKey<T1, T2>
    {
        public CompositeKey(T1 key1, T2 key2)
        {
            Key1 = key1;
            Key2 = key2;
        }

        public CompositeKey()
        {
        }

        public T1 Key1 { get; set; }
        public T2 Key2 { get; set; }

        protected bool Equals(CompositeKey<T1, T2> other)
        {
            return EqualityComparer<T1>.Default.Equals(Key1, other.Key1) && EqualityComparer<T2>.Default.Equals(Key2, other.Key2);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((CompositeKey<T1, T2>) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (EqualityComparer<T1>.Default.GetHashCode(Key1) * 397) ^ EqualityComparer<T2>.Default.GetHashCode(Key2);
            }
        }

        public override string ToString()
        {
            return $"{Key1} {Key2}";
        }
    }
}