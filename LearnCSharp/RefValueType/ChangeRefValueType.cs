﻿using System;
namespace LearnCSharp.RefValueType
{
   public class ChangeRefValueType
    {
        public void ChangeRefValueRefType()
        {
            var refValueType = new RefValueType();
            int i = 5, k = 10;
            Console.WriteLine($"{i},{k}");
            var refType = new PersonName {Name = "Bogdan", Age = 21};
            var refPerson = new PersonName {Name = "Bob", Age = 31};
            Console.WriteLine($"{refType.Name},{refType.Age};{refPerson.Name},{refPerson.Age}");
            string s = "Dog";
            refValueType.ValueReferenceRefValueRefReferenceTypePass(i, refType, ref k, ref refPerson,"Kit",ref s);
        //    Console.WriteLine($"{i};{refType.Name},{refType.Age};{k};{refPerson.Name},{refPerson.Age},{} ");
        }
    }
}
