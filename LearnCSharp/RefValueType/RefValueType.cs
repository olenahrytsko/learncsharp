﻿using System;

namespace LearnCSharp.RefValueType
{
   public class RefValueType
    {
        public void ValueReferenceRefValueRefReferenceTypePass(int valueType, PersonName person, ref int refValueType,
            ref PersonName refPerson,string a, ref string b) //передаючи по референсу можна змінити те що під референсом
        {
            valueType += 10;
            refValueType += 10;
            Console.WriteLine(
                $"{valueType},{person.Name}, {person.Age},{refValueType},{refPerson.Name}, {refPerson.Age}");
            person = new PersonName {Age = 20, Name = "Olena"};
            person.Age = 40;
            refPerson = new PersonName {Age = 30, Name = "Ira"};
            string c = "Olena";
            string d = "Jon";
            b =c;
             a=d;
            Console.WriteLine(
                $"{valueType},{person.Name}, {person.Age},{refValueType},{refPerson.Name}, {refPerson.Age},{a},{b}{c}");
        }
    }
}
