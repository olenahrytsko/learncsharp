﻿using System.Collections.Generic;

namespace LearnCSharp.DataStructure
{
    public interface ITraverse<out T>
    {
        IEnumerable<T> TraverseInfix();

        IEnumerable<T> TraversePostFix();
        
        IEnumerable<T> TraversePrefix();
    }
}