﻿using System.Collections.Generic;

namespace LearnCSharp.DataStructure
{
    public class GraphNode<TKey>
    {
        private List<GraphNode<TKey>> list;

        public GraphNode(TKey key)
        {
            Key = key;
            list = new List<GraphNode<TKey>>();
        }

        public TKey Key { get; }

        public IList<GraphNode<TKey>> List => list;

        public override string ToString()
        {
            return $"{Key}";
        }
    }
}
