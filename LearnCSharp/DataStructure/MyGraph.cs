﻿using System;
using System.Collections.Generic;

namespace LearnCSharp.DataStructure
{
    public class MyGraph<TKey>
    {
        private List<GraphNode<TKey>> nodes = new List<GraphNode<TKey>>();

        public void Add(GraphNode<TKey> node)
        {
            nodes.Add(node);
        }

        public IEnumerable<GraphNode<TKey>> BFS(GraphNode<TKey> start)
        {
            var visited = new HashSet<GraphNode<TKey>>();
            var queue = new MyQueue<GraphNode<TKey>>();
            queue.Add(start);
            while (queue.Count != 0)
            {
                var node = queue.Pop();
                if (visited.Contains(node)) continue;
                yield return node;
                visited.Add(node);
                for (int i = 0; i < node.List.Count; i++)
                {
                    if (!visited.Contains(node.List[i]))
                        queue.Add(node.List[i]);
                }
            }
        }

        public IEnumerable<GraphNode<TKey>> DFS(GraphNode<TKey> start)
        {
            var visited = new HashSet<GraphNode<TKey>>();
            var stack = new MyStack<GraphNode<TKey>>();
            stack.Add(start);
            while (stack.Count != 0)
            {
                var node = stack.Pop();
                if (!visited.Add(node)) continue;
                yield return node;
                for (int i = 0; i < node.List.Count; i++)
                {
                    if (!visited.Contains(node.List[i]))
                        stack.Add(node.List[i]);
                }
            }
        }

        public List<GraphNode<TKey>> Nodes
        {
            get { return nodes; }
        }
    }
}