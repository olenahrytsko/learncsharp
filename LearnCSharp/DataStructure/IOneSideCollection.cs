﻿namespace LearnCSharp.DataStructure
{
    public interface IOneSideCollection<T>
    {
        void Add(T Item);
        T Pop();

        T Peek();
        int Count { get; }

    }
}