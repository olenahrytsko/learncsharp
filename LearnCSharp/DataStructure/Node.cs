﻿using System.Collections.Generic;

namespace LearnCSharp.DataStructure
{
    public class  Node<T>
    {
        public Node(T item, Node<T> next)
        {
            Item = item;
            Next = next;
        }

        public T Item { get; set; }

        public Node<T> Next { get; set; }

        public IEnumerable<T> GetAll()
        {
            yield return Item;
            if (Next != null)
                foreach (var item in Next.GetAll())
                {
                    yield return item;
                }
        }

        public override string ToString()
        {
            return $"{Item}";
        }
    }
}