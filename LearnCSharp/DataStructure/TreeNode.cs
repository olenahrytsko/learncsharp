﻿namespace LearnCSharp.DataStructure
{
    public class TreeNode<TKey, TValue>
    {
        public TreeNode(TKey key, TValue value, TreeNode<TKey, TValue> parent, TreeNode<TKey, TValue> left, TreeNode<TKey, TValue> right, int height)
        {
            Key = key;
            Value = value;
            Parent = parent;
            Left = left;
            Right = right;
            Height = height;
        }

        public TreeNode<TKey, TValue> Parent { get; set; }

        public TreeNode<TKey, TValue> Right { get; set; }

        public TreeNode<TKey, TValue> Left { get; set; }

        public TKey Key { get; set; }

        public TValue Value { get; set; }

        public int Height { get; set; }

        public override string ToString()
        {
            return $"{Key}";
        }
    }
}
  