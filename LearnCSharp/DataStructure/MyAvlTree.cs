﻿using System;

namespace LearnCSharp.DataStructure
{
    public class MyAvlTree<TKey, TValue> : MyTree<TKey, TValue> where TKey : IComparable<TKey>
    {
        public override void Add(TKey key, TValue value)
        {
            if (root == null)
            {
                root = NewNode(key, value);
                Count++;
                return;
            }

            root = AddInternal(root, key, value);
            Count++;
        }

        public TreeNode<TKey, TValue> AddInternal(TreeNode<TKey, TValue> node, TKey key, TValue value)
        {
            if (node == null)
                return (NewNode(key, value));

            if (node.Key.CompareTo(key) == 1)
            {
                node.Left = node.Left == null ? new TreeNode<TKey, TValue>(key, value, node, null, null, 1) : AddInternal(node.Left, key, value);
            }
            else if (node.Key.CompareTo(key) == -1)
            {
                node.Right = node.Right == null ? new TreeNode<TKey, TValue>(key, value, node, null, null, 1) : AddInternal(node.Right, key, value);
            }
            else
                return node;
            // 2. Update height 
            node.Height = 1 + Math.Max(Height(node.Left), Height(node.Right));
            /* 3. Get the balance factor of this ancestor 
                              node to check whether this node became 
                              unbalanced */
            var balance = Balance(node);
            // Left Left Case 
            if (balance > 1 && node.Left.Key.CompareTo(key) == 1)
                return RightRotate(node);
            // Right Right Case 
            if (balance < -1 && node.Right.Key.CompareTo(key) == -1)
                return LeftRotate(node);
            // Left Right Case 
            if (balance > 1 && node.Left.Key.CompareTo(key) == -1)
            {
                node.Left = LeftRotate(node.Left);
                return RightRotate(node);
            }
            // Right Left Case 
            if (balance < -1 && node.Right.Key.CompareTo(key) == 1)
            {
                node.Right = RightRotate(node.Right);
                return LeftRotate(node);
            }
            return node;
        }

        public int Height(TreeNode<TKey, TValue> node)
        {
            return node == null ? 0 : node.Height;
        }

        public TreeNode<TKey, TValue> RightRotate(TreeNode<TKey, TValue> node)
        {
            TreeNode<TKey, TValue> nodeRight = node.Left;
            TreeNode<TKey, TValue> nodeLeft = nodeRight.Right;
            nodeRight.Right = node;
            node.Left = nodeLeft;
            nodeRight.Parent = node.Parent;
            node.Parent = nodeRight;
            if (nodeLeft != null)
                nodeLeft.Parent = node;
            node.Height = Math.Max(Height(node.Left), Height(node.Right)) + 1;
            nodeRight.Height = Math.Max(Height(nodeRight.Left), Height(nodeRight.Right)) + 1;
            return nodeRight;
        }

        public TreeNode<TKey, TValue> LeftRotate(TreeNode<TKey, TValue> node)
        {
            TreeNode<TKey, TValue> nodeLeft = node.Right;
            TreeNode<TKey, TValue> nodeRight = nodeLeft.Left;
            nodeLeft.Left = node;
            node.Right = nodeRight;
            nodeLeft.Parent = node.Parent;
            node.Parent = nodeLeft;
            if (nodeRight != null)
                nodeRight.Parent = node;
            node.Height = Math.Max(Height(node.Left), Height(node.Right)) + 1;
            nodeLeft.Height = Math.Max(Height(nodeLeft.Left), Height(nodeLeft.Right)) + 1;
            return nodeLeft;
        }

        public int Balance(TreeNode<TKey, TValue> node)
        {
            if (node == null)
                return 0;
            return Height(node.Left) - Height(node.Right);
        }

        public TreeNode<TKey, TValue> NewNode(TKey key, TValue value)
        {
            var avlNode = new TreeNode<TKey, TValue>(key, value, null, null, null, 1);
            return (avlNode);
        }

        public override void Remove(TKey key)
        {
            root = DeleteInternal(root, key);
        }

        public TreeNode<TKey, TValue> DeleteInternal(TreeNode<TKey, TValue> node, TKey key)
        {
            base.Remove(key);

            // STEP 2: UPDATE HEIGHT OF THE CURRENT NODE 
            node.Height = 1 + Math.Max(Height(node.Left), Height(node.Right));

            // STEP 3: GET THE BALANCE FACTOR OF THIS NODE (to 
            // check whether this node became unbalanced) 
            var balance = Balance(node);
            // Left Left Case 
            if (balance > 1 && Balance(node.Left) >= 0)
                node= RightRotate(node);
            // Left Right Case 
            if (balance > 1 && Balance(node.Left) < 0)
            {
                node.Left = LeftRotate(node.Left);
                return RightRotate(node);
            }
            // Right Right Case 
            if (balance < -1 && Balance(node.Right) <= 0)
                return LeftRotate(node);
            // Right Left Case 
            if (balance < -1 && Balance(node.Right) > 0)
            {
                node.Right = RightRotate(node.Right);
                return LeftRotate(node);
            }

            return node;
        }
    }
}