﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace LearnCSharp.DataStructure
{
    public class MyList<T> : IList<T>
    {
        private T[] array;
        private const int defaultSize = 4;

        public MyList()
        {
            array = new T[defaultSize];
        }

        public void Add(T item)
        {
            if (array.Length == Count)
                Array.Resize(ref array, Count * 2);
            array[Count++] = item;
        }

        public void Clear()
        {
            for (int i = Count-1; i >=0; i--)
            {
                RemoveAt(i);
            }
        }

        public bool Contains(T item)
        {
            for (int i = 0; i < Count; i++)
            {
                if (array[i].Equals(item))
                {
                    return true;
                }
            }
            return false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool Remove(T item)
        {
            throw new NotImplementedException();
        }

        public int IndexOf(T item)
        {
            for (int i = 0; i < Count; i++)
            {
                if (array[i].Equals(item))
                {
                    return i;
                }
            }
             return -1;
        }

        public void Insert(int index, T item)
        {
            Add(item);
            for (int i = index; i < Count - 1; i++)
            {
                ListHelper.Swap(this, i, Count - 1);
            }
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index >= Count)
            {
                throw new IndexOutOfRangeException();
            }

            Array.Copy(array, index + 1, array, index, Count - index - 1);
            array[Count - 1] = default(T);
            Count--;
        }

        public T this[int index]
        {
            get
            {
                if (index < 0 || index >= Count)
                    throw new IndexOutOfRangeException();
                return array[index];
            }
            set
            {
                if (index < 0 || index >= Count)
                       throw new IndexOutOfRangeException();
                array[index] = value;
            }

        }

        public int Count { get; private set; }
        public bool IsReadOnly => false;

        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerable<T> GetAll()
        {
            for (int i = 0; i < Count; i++)
            {
                yield return array[i];
            }
        }
    }
}