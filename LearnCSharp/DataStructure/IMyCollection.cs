﻿namespace LearnCSharp.DataStructure
{
    public interface IMyCollection<T>
    {
        void Add(T item);

        void Remove(int index);

        T this[int index] { get; set; }

        int Count { get; }
    }
}