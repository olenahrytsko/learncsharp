﻿using System;
using System.Collections.Generic;

namespace LearnCSharp.DataStructure
{
    public class MyTree<TKey, TValue> : ITree<TKey, TValue>, ITraverse<TreeNode<TKey, TValue>>
        where TKey : IComparable<TKey>
    {
        protected TreeNode<TKey, TValue> root;

        public virtual void Add(TKey key, TValue value)
        {
            if (root == null)
            {
                var treeNode = new TreeNode<TKey, TValue>(key, value, null, null, null,1);
                root = treeNode;
                Count++;
                return;
            }

            AddInternal(root, key, value);
            Count++;
        }

        private static void AddInternal(TreeNode<TKey, TValue> node, TKey key, TValue value)
        {
            if (node.Key.Equals(key)) //if(root.Key.CompareTo(key) == 0)
            {
                throw new Exception("Key already exist!");
            }

            if (node.Key.CompareTo(key) == 1)
            {
                if (node.Left == null)
                {
                    node.Left = new TreeNode<TKey, TValue>(key, value, node, null, null,1);
                }
                else
                {
                    node = node.Left;
                    AddInternal(node, key, value);
                }
            }

            else if (node.Key.CompareTo(key) == -1)
            {
                if (node.Right == null)
                {
                    node.Right = new TreeNode<TKey, TValue>(key, value, node, null, null,1);
                }
                else
                {
                    node = node.Right;
                    AddInternal(node, key, value);
                }
            }
        }

        public virtual void Remove(TKey key)
        {
            if (root == null)
                return;
            var findNode = Find(key);
            if (findNode.Key.CompareTo(root.Key) == 0)
            {
                RemoveRoot(findNode);
                Count--;
                return;
            }

            RemoveNode(findNode);
            Count--;
        }

        private void RemoveNode(TreeNode<TKey, TValue> node)
        {
            if (node.Left == null && node.Right == null)
            {
                if (node.Parent.Key.CompareTo(node.Key) == -1)
                    node.Parent.Right = null;
                else
                    node.Parent.Left = null;
                return;
            }
            if (node.Left == null && node.Right != null)
            {
                if (node.Parent.Key.CompareTo(node.Right.Key) == -1)
                {
                    node.Parent.Right = node.Right;
                    node.Right.Parent = node.Parent; //check
                }
                else
                {
                    node.Parent.Left = node.Right;
                    node.Right.Parent = node.Parent; //check
                }
                return;
            }
            if (node.Right == null && node.Left != null)
            {

                if (node.Parent.Key.CompareTo(node.Left.Key) == 1)
                {
                    node.Parent.Left = node.Left;
                    node.Left.Parent = node.Parent;
                }
                else
                {
                    node.Parent.Right = node.Left;
                    node.Left.Parent = node.Parent;
                }
                return;
            }
            if (node.Right != null && node.Left != null)
            {
                RemoveNodeWithTwoChild(node);
            }
        }

        private void RemoveNodeWithTwoChild(TreeNode<TKey, TValue> node)
        {
            if (node.Right.Left == null)
            {
                node.Key = node.Right.Key;
                node.Value = node.Right.Value;
                node.Right = node.Right.Right;
            }
            else
            {
                var lastLeftNode = GetLastLeftNode(node.Right);
                node.Key = lastLeftNode.Key;
                node.Value = lastLeftNode.Value;
                RemoveNode(lastLeftNode);
            }
        }


        private void RemoveRoot(TreeNode<TKey, TValue> node)
        {
            if (node.Left == null && node.Right == null)
            {
                root = null;
                return;
            }
            if (node.Left == null && node.Right != null)
            {

                root = node.Right;
               return;
            }
            if (node.Right == null && node.Left != null)
            {
                root = node.Left;
                return;
            }
            if (node.Right != null && node.Left != null)
            {
                RemoveNodeWithTwoChild(node);
            }
        }

        private TreeNode<TKey, TValue> GetLastLeftNode(TreeNode<TKey, TValue> node)
        {
            while (node.Left != null)
            {
                node = node.Left;
            }
            return node;
        }

        public TreeNode<TKey, TValue> Find(TKey key)
        {
            if (root == null)
            {
                throw new Exception("Node not found!");
            }
            TreeNode < TKey, TValue > node= FindNode(root, key);
            return node;
        }

        private TreeNode<TKey, TValue> FindNode(TreeNode<TKey, TValue> node, TKey key)
        {
            var comp = node.Key.CompareTo(key);

            if (comp == 1)
            {
                node = node.Left;
                if (node == null)
                {
                    throw new Exception("Node not found");
                }

                return FindNode(node, key);
            }

            if (comp == -1)
            {
                node = node.Right;
                if (node == null)
                {
                    throw new Exception("Node not found");
                }

                return FindNode(node, key);
            }

            if (comp == 0)
                return node;

            throw new Exception("Node not found");
        }


        public int Count { get; set; }

        public IEnumerable<TreeNode<TKey, TValue>> TraverseInfix()
        {
            if (root == null)
                yield break;
            foreach (var node in TraverseInfixInternal(root))
            {
                yield return node;
            }
        }

        private IEnumerable<TreeNode<TKey, TValue>> TraverseInfixInternal(TreeNode<TKey, TValue> treeNode)
        {
            if (treeNode == null)
                yield break;
            foreach (var node in TraverseInfixInternal(treeNode.Left))
                yield return node;
            yield return treeNode;
            foreach (var node in TraverseInfixInternal(treeNode.Right))
                yield return node;
        }

        public IEnumerable<TreeNode<TKey, TValue>> TraversePostFix()
        {
            if (root == null)
                yield break;
            foreach (var node in TraversePostFixInternal(root))
            {
                yield return node;
            }
        }
        private static IEnumerable<TreeNode<TKey, TValue>> TraversePostFixInternal(TreeNode<TKey, TValue> treeNode)
        {
            if (treeNode == null)
                yield break;
            foreach (var node in TraversePostFixInternal(treeNode.Left))
                yield return node;
            foreach (var node in TraversePostFixInternal(treeNode.Right))
                yield return node;
            yield return treeNode;
        }

        public IEnumerable<TreeNode<TKey, TValue>> TraversePrefix()
        {
            if (root == null)
                yield break;
            foreach (var node in TraversePrefixInternal(root))
            {
                yield return node;
            }
        }

        private static IEnumerable<TreeNode<TKey, TValue>> TraversePrefixInternal(TreeNode<TKey, TValue> treeNode)
        {
            if (treeNode == null)
                yield break;
            yield return treeNode;
            foreach (var node in TraversePrefixInternal(treeNode.Left))
                yield return node;
            foreach (var node in TraversePrefixInternal(treeNode.Right))
                yield return node;
        }
    }
}