﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace LearnCSharp.DataStructure
{
    public class MyLinkedList<T> : IList<T>
    {
        private Node<T> head;

        public void Add(T item)
        {
            Node<T> node = new Node<T>(item, head);
            head = node;
            Count++;
        }

        public void Clear()
        {
            head = null;
        }

        public bool Contains(T item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        private TResult Iterate<TResult>(T item, Func<int, TResult> found, Func<TResult> notFound)
        {
            var node = head;
            int counter = 0;
            while (node != null)
            {
                if (node.Item.Equals(item))
                {
                    return found(counter);
                }

                counter++;
                node = node.Next;
            }

            return notFound();
        }

        public bool Remove(T item)
        {
            return Iterate(item, n =>
            {
                RemoveAt(n);
                return true;
            }, () => false);
        }

        public int IndexOf(T item)
        {
            return Iterate(item, n => n, () => -1);
        }

       
        public void Insert(int index, T item)
        {
            if (index < 0 || index >= Count)
            {
                throw new IndexOutOfRangeException();
            }

            if (index == 0)
            {
                var newNode = new Node<T>(item, head);
                head = newNode;
                Count++;
                return;
            }

            Iterate(index, n =>
            {
                var newNode = new Node<T>(item, n.Next);
                n.Next = newNode;
            });
            Count++;
        }

        private void Iterate(int index, Action<Node<T>> action)
        {
            var node = head;
            int counter = 0;
            while (node.Next != null)
            {
                if (counter++ == index - 1)
                {
                    action(node);
                    break;
                }

                node = node.Next;
            }
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index >= Count)
            {
                throw new IndexOutOfRangeException();
            }
            if (index == 0)
            {
                head = head.Next;
                Count--;
                return;
            }
            Iterate(index, n =>
            {
                var nodePrevious = n.Next;
                n.Next = nodePrevious.Next;
            });
           Count--;
        }

        public T this[int index]
        {
            get
            {
                if (index < 0 || index >= Count)
                    throw new IndexOutOfRangeException();
                return GetItem(index, head);
            }
            set
            {
                if (index < 0 || index >= Count)
                    throw new IndexOutOfRangeException();
                SetItem(index, head, value);
            }
        }

        private static T GetItem(int index, Node<T> node)
        {
            int counter = 0;
            while (node.Next != null)
            {
                if (counter++ == index)
                {
                    return node.Item;
                }

                node = node.Next;
            }

            return node.Item;
        }

        private void SetItem(int index, Node<T> node, T item)
        {
            if (node.Next == null)
            {
                node.Item = item;
            }

            Iterate(index, n =>
            {
                n.Next.Item = item;
            });
        }

        public int Count { get; set; }
        public bool IsReadOnly => false;


        public IEnumerable<T> GetAll()
        {
            return head.GetAll();
        }

        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}