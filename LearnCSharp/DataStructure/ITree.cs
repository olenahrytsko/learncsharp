﻿using System;

namespace LearnCSharp.DataStructure
{
    public interface ITree<TKey, TValue>
        where TKey : IComparable<TKey>
    {
        void Add(TKey key, TValue value);

        void Remove(TKey key);

        TreeNode<TKey, TValue> Find(TKey key);
         int Count { get; set; }
    }
}