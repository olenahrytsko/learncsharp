﻿namespace LearnCSharp.DataStructure
{
    public class MyQueue<T>: IOneSideCollection<T>
    {
        private Node<T> head;

        public void Add(T item)
        {
            if (head == null)
            {
                Node<T> node = new Node<T>(item, head);
                head = node;
                Count++;
                return;
            }
            else
            {
                var node = head;
                while (node.Next != null||node.Item!=null)
                {
                    if (node.Next == null)
                    {
                        var newNode = new Node<T>(item, node.Next);
                        node.Next = newNode;
                        break;
                    }

                    node = node.Next;
                }
                Count++;
            }
        }

        public T Pop()
        {
            var newNode = head;
            head = head.Next;
            Count--;
            return newNode.Item;
        }

        public T Peek()
        {
            return head.Item;
        }

        public int Count { get; private set;}

       
    }
}