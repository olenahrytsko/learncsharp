﻿namespace LearnCSharp.DataStructure
{
    public class MyStack<T>: IOneSideCollection<T>
    {
        private Node<T> head;

        public void Add(T item)
        {
            Node<T> node = new Node<T>(item, head);
            head = node;
            Count++;
        }

        public T Peek()
        {
            return head.Item;
        }

        public T Pop()
        {
            var newNode = head;
            head = head.Next;
            Count--;
            return newNode.Item;
        }

        public int Count { get; private set; }
    }


}