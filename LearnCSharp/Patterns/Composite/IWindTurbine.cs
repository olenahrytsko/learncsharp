﻿using System;

public interface IWindTurbine
{
    double GetPower();
}

