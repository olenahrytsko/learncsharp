﻿using System;
using System.Collections.Generic;
using System.Linq;

public class Park : IWindTurbine
{
	private IWindTurbine[] windTurbines;

	public Park(params IWindTurbine[] windTurbines)
	{
		this.windTurbines = windTurbines;
	}

	public double GetPower()
	{
		return windTurbines.Sum(a => a.GetPower());
	}
}