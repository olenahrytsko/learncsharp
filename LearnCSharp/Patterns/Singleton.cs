﻿namespace LearnCSharp.Patterns
{
    public class Singleton
    {
        private static Singleton instatnce;

        private Singleton()
        {

        }

        public static Singleton GetInstance()
        {
            if (instatnce==null)
                instatnce=new Singleton();
            return instatnce;

        }
    }
}