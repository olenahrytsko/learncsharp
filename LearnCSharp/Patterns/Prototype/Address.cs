﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnCSharp.Patterns.Prototype
{
    public class Address
    {
        public string City { get; set; }
        public string Country { get; set; }

        public Address DeepCopy()
        {
            var clone = (Address)this.MemberwiseClone();
            return clone;
        }
    }
}
