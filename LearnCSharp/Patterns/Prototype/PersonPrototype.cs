﻿
using System.Runtime;
using System;

namespace LearnCSharp.Patterns.Prototype
{
    public class PersonPrototype
    {
        public int Age { get; set; }
        public DateTime BirthDate { get; set; }
        public string Name { get; set; }
        public Address Address { get; set; }
        public PersonPrototype ShallowCopy()
        {
            return (PersonPrototype)this.MemberwiseClone();
        }

        public PersonPrototype DeepCopy()
        {
            var clone = (PersonPrototype)this.MemberwiseClone();
            clone.Address = Address.DeepCopy();
            return clone;
        }
    }
}
