﻿using System;
using System.Collections.Generic;
using LearnCSharp.LINQ;


namespace LearnCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            var k = new RunLinq();
            k.Run();

            //var s1 = "Hello";
            //int myvar = 10;
            //object myObj = myvar;
            //var s2 = s1;
            //s1 = "World";
            //Console.WriteLine($"s1={s1},s2={s2}");

            //var p1 = new Person() {Name = "Hello"};
            //var p2 = p1;
            //p1.Name = "World";
            //Console.WriteLine($"p1={p1.Name},p2={p2.Name}");

            //var c1 = new MyStruct();
            //c1.X = 1;
            //var c2 = c1;
            //c1.X = 2;
            //Console.WriteLine($"p1={c1.X},p2={c2.X}");
            //var person = new PersonName { Age = 10, Name = "Bogdan" };
            //var refPerson = new PersonName { Age = 15, Name = "Jon" };
            //RefValueType.RefValueType refVal = new RefValueType.RefValueType();
            //int i = 1;
            //refVal.ValueReferenceRefValueRefReferenceTypePass(1,person, ref i, ref refPerson);
            //var chRefValnew = new ChangeRefValueType();
            //chRefValnew.ChangeRefValueRefType();
            /*var subsets = new GeneratSubsets();
             subsets.PrintSubsets();*/
            // var run =new InheritanceRun();
            //  var run= new PersonEquals();
            //  var run=new TryExeption();
            //var run=new RunAction();
            //    var run = new RunLinq();
            //    run.Run();
            //run.LogRun();
            //     run.PropertyExeption();
            //    run.MyExeption();
            //   run.Run();
            //MyStaticClass.Ping();
            ///     var run = new RunOneWayLinkedList();
            //     run.Run();
            //  int s = Sum(-1,101);
            // var a=new InheritanceRun();
            //  a.Run();
            //var f = Factorial(5);
            //Console.WriteLine($"f={f}");
            //   var a = "Hello";
            //   var b = 1;
            //   var c =0.5;
            //   var res = string.Format("{0}, {1}", a, b, c);
            //   Console.WriteLine(res);
            //   var list = new List<int>() { 1, 2, 3, 4 };
            //   Swap(list, 3, 0);
            //   foreach (var VARIABLE in list)
            //   {
            //       Console.WriteLine(VARIABLE);
            //   }

            //   var linkedlist = new LinkedList<int>();
            //   linkedlist.AddLast(4);
            //   linkedlist.AddFirst(8);
            //var f=  Factorial(4);

            // var a = GenerateWithYield().Take(4).ToList();

            //   var b = GenerateWithoutYield();
            //   dynamic dynamicVariable = "1";

            //   Console.WriteLine(dynamicVariable.GetType().ToString());
            //   var i = 5;
            //var s = Convert.ToString(i);
            //   //var list = new List<int>();
            //   //for (int j = 0; j < 10; j++)
            //   //{
            //   //    list.Add(j);
            //   //}
            //   var list = new List<PersonName>();
            //   list.Add(new PersonName(){ Name = "Olena", Age=23});
            //   list.Add(new PersonName() { Name = "Olena", Age = 32 });
            //   list.Add(new PersonName() { Name = "Bogdan", Age = 32 });

            //   var res = new Dictionary<string, List<PersonName>>();
            //   var p =  list.GroupBy(a=>a.Name, a=>a.Age).ToList();


            //   //var k = "n";
            //   //int h = Convert.ToInt32(k);
            //   //Console.WriteLine($"{h}");
            //   foreach (var item in list)
            //   {
            //       if (res.ContainsKey(item.Name))
            //       {
            //           var list2 = res[item.Name];
            //           list2.Add(new PersonName() { Name = "Olena", Age = 23 });

            //       }
            //       else
            //       {
            //           var list3 = new List<PersonName>();
            //           list3.Add(new PersonName() { Name = "Olena", Age = 23 });
            //           res.Add(item.Name, list3);
            //       }


            //   }


            //var k = ListWithYield<int>(list).Max();
            //var c = ListWithYield<int>(list).ToList();

        }

        public static void  MaxVal()
        {
            int[] array = { 1, 5, 2, 7 };
            int maxIndex = -1;
            int maxInt = Int32.MinValue;

            // Modern C# compilers optimize the case where we put array.Length in the condition
            for (int i = 0; i < array.Length; i++)
            {
                int value = array[i];
                if (value > maxInt)
                {
                    maxInt = value;
                    maxIndex = i;
                }
            }
        }
        static IEnumerable<T> ListWithYield<T>(List<T> list)
        {
            var i = 0;
            foreach (var item in list)
            {
                if (i % 3 == 2)
                    yield return item;
                i++;
            }
        }
        static IEnumerable<int> GenerateWithYield() 
        { 
            var i = 0; 
            while (true) 
                yield return ++i; 
        }
       static IEnumerable<int> GenerateWithoutYield() 
        { 
            var i = 0; 
            var list = new List<int>(); 
            while (true) 
                list.Add(++i); 
            return list; 
        }

        public static void Swap<T>(List<T> list, int indexA, int indexB)
        {
            var tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
        }

        public static int Factorial(int n)
        {
            if (n == 1)
            {             
                return 1;
            }
                return n * Factorial(n - 1);
        }

    }
}