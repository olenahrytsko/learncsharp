﻿using System;
using System.Collections.Generic;

namespace LearnCSharp.LINQ
{
    public class RunLinq
    {
        public void Run()
        {
            var funLinq=new FunWithLinq();
            var listProducts = new List<ProductInfo>();
            listProducts=funLinq.CreateLict();
            funLinq.SelectEverything(listProducts);
            Console.WriteLine("-----------------------");
            funLinq.SelectName(listProducts);
            Console.WriteLine("-----------------------");
            funLinq.GetOverstock(listProducts);
            Console.WriteLine("-----------------------");
            funLinq.GetCount(listProducts);
            Console.WriteLine("-----------------------");
            funLinq.SelectMany(listProducts);
        }
    }
}