﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LearnCSharp.LINQ
{
    public class FunWithLinq
    {
        public List<ProductInfo> CreateLict()
        {
            List<ProductInfo> itemsInStock = new List<ProductInfo>
            {
                new ProductInfo {Name = "Mac Coffee", Description = "Coffee with TEETH", NumberInStock = 24},
                new ProductInfo {Name = "Milk Maid Milk", Description = "Milk cows love", NumberInStock = 100},
                new ProductInfo {Name = "Pure Silc Tofu", Description = "Bland as Possible", NumberInStock = 120},
                new ProductInfo {Name = "Cruchy Pops", Description = "Cheezy, peppert goodness", NumberInStock = 2},
                new ProductInfo {Name = "RipOff Water", Description = "From the tap your wallet", NumberInStock = 100},
                new ProductInfo {Name = "RipOff Bask", Description = "From the tap your wallet", NumberInStock = 100},

            };
            return itemsInStock;
        }

        public void SelectEverything(List<ProductInfo> products)
        {
            Console.WriteLine("All products details:");
            var allProducts = from a in products orderby a.Name descending select a;
            foreach (var item in allProducts)
            {
                Console.WriteLine(item.ToString());
            }
        }
        public void SelectName(List<ProductInfo> products)
        {
            Console.WriteLine("Products name:");
            var allProducts = from p in products select p.Name;
            foreach (var item in allProducts)
            {
                Console.WriteLine(item.ToString());
            }
        }
        public void GetOverstock(List<ProductInfo> products)
        {
            Console.WriteLine("The overstock item:");
            var allProducts = from p in products where p.NumberInStock > 25 select p;
            foreach (var item in allProducts)
            {
                Console.WriteLine(item.ToString());
            }
        }
        public void GetCount(List<ProductInfo> products)
        {
            Console.WriteLine("Number of the overstock item:");
            int numb = (from p in products where p.NumberInStock > 25 select p).Count();
                Console.WriteLine(numb);

        }
        public void SelectMany(List<ProductInfo> products)
        {
            Console.WriteLine("select many item:");
            var k =products.SelectMany(a => a.Description).ToList();
            var f = products.Select(a => a.Description).ToList();
            foreach (var item in k)
            {
                Console.WriteLine(item.ToString());
            }
            Console.WriteLine("select  item:");
            foreach (var item in f)
            {
                Console.WriteLine(item.ToString());
            }

        }
    }
}