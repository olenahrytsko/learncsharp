﻿using System;

namespace LearnCSharp.AccessModifier
{
    public class AccessModifiers
    {
        public AccessModifiers()
        {
            MyPublicProp = 1;
            MyPrivateProp = 2;
            MyProtectedProp = 3;
            MyInternalProp = 4;
            MyProtectedInternalProp = 5;
        }

        public int MyPublicProp { get; private set; }

        private int MyPrivateProp { get; set; }

        protected int MyProtectedProp { get; private set; }

        internal int MyInternalProp { get; private set; }

        protected internal int MyProtectedInternalProp { get; private set; }

        public void Call()
        {
            var am = new AccessModifiersPrivate();
            Console.WriteLine(am.PrivatePublic);
            //Console.WriteLine(am.PrivatePrivate);
        }

        private class AccessModifiersPrivate
        {
            public int PrivatePublic { get; set; }

            private int PrivatePrivate { get; set; }

            public void Call()
            {
                var am = new AccessModifiers();
                Console.WriteLine(am.MyInternalProp);
                Console.WriteLine(am.MyProtectedInternalProp);
                Console.WriteLine(am.MyPublicProp);
                Console.WriteLine(am.MyPrivateProp);
                Console.WriteLine(am.MyProtectedProp);
            }
        }
    }
}