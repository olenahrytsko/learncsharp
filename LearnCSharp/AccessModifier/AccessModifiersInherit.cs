﻿using System;

namespace LearnCSharp.AccessModifier
{
    public class AccessModifiersInherit : AccessModifiers
    {
        public void Call()
        {
            Console.WriteLine(MyInternalProp);
            Console.WriteLine(MyProtectedInternalProp);
            Console.WriteLine(MyPublicProp);
            //Console.WriteLine(MyPrivateProp);
            Console.WriteLine(MyProtectedProp);
        }
    }
}