﻿using System.Xml;
using LearnCSharp.DataStructure;

namespace LearnCSharp.List
{
    public class NodeList
    {
        public int Value { get; set; }
        public NodeList Next { get; set; }
        public NodeList(int value, NodeList next)
        {
            Value = value;
            Next = next;
        }

        public override string ToString()
        {
            return $"{Value}";
        }

    }
}