﻿namespace LearnCSharp.List
{
    public class OneWayLinkedList
    {
        private NodeList head;
        public int Count { get; set; }

        public void Add(int value)
        {
            var node=new NodeList(value,head);
            head = node;
            Count++;
        }
    }
}