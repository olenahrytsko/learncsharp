﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace LearnThreading
{
    class Program
    {
        private static object lockObj = new object();

        static void Main(string[] args)
        {
            //  MultiThreadingUsingTasks();
            //Parralelfor();
            float a = 0.0003f;
            decimal b = 0.008M;
            int[] arr = {1, 2, 3, 4};
            var d = new int[6];// {1, 2, 3, 4,5 };

            int[,,] array3Da = new int[2, 2, 3] { { { 1, 2, 3 }, { 4, 5, 6 } },
                { { 7, 8, 9 }, { 10, 11, 12 } } };

            var stesk = new Stack<int>();
            stesk.Push(2);
        }

        private static void Parralelfor()
        {
            long sum = 0;
            Parallel.For(1, 2000000000, number => sum += number);
            Console.WriteLine(sum);
        }

        private static void MultiThreadingUsingTasks()
        {
            var t = new Stopwatch();
            t.Start();
            Task<long> task1 = new Task<long>(() => Sum(1, 1000000000));
            task1.Start();
            Task<long> task2 = new Task<long>(() => Sum(1000000000, 2000000000));
            task2.Start();
            var sum = task1.Result + task2.Result;
            Console.WriteLine($"RunTime {t.Elapsed}, sum : {sum}");
            Console.ReadKey();
        }

        public static long Sum(int lover, int upper)
        {
            //lock (lockObj)
            {
                Console.WriteLine($"Started {DateTime.UtcNow}");
                long sum = 0;
                for (var i = lover; i < upper; i++)
                {
                    sum += i;
                }

                return sum;
            }
        }
    }
}
