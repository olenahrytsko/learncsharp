﻿using System.Collections.Generic;

namespace WebAPIPhone.Dto
{
    public class PersonDto
    {
        public List<CityDto> Cities { get; set; }
        public string Name { get; set; }
        public string Nickname { get; set; }
        public string Surname { get; set; }
     // public string City { get; set; }
        public string PhoneNumber { get; set; }
        public int PersonId { get; set; }

        public PersonDto(int id, string name, string surname, string nickname, List<CityDto> city, string phoneNumber)
        {
            PersonId = id;
            Name = name;
            Surname = surname;
            Nickname = nickname;
            Cities = city;
            PhoneNumber = phoneNumber;
        }
        public PersonDto()
        {
        }
    }
}