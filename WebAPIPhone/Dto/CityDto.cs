﻿namespace WebAPIPhone.Dto
{
    public class CityDto
    {
        public string Name { get; set; }
        public int CityId { get; set; }
        public CityDto(string name,int id)
        {
            Name = name;
            CityId = id;
        }
        public override string ToString()
        {
            return $"{Name}";
        }
    }
}