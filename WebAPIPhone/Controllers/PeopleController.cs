﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LearnCSharp.DataStructureTask;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LearnCSharpEFCore;
using LearnCSharpEFCore.Model;
using Newtonsoft.Json;
using WebAPIPhone.Dto;

namespace WebAPIPhone.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        private readonly ApplicationContext _context;

        public PeopleController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: api/People
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PersonDto>>> GetPersons()
        {
            var persons = await _context.Persons.Include(order => order.PersonCities)
                .ThenInclude(orderProducts => orderProducts.City).ToListAsync();
            //return await _context.Persons.ToListAsync();
            List<PersonDto> list = new List<PersonDto>();
           
           foreach (var item in persons)
            {
                List<CityDto> listCity = new List<CityDto>();
                foreach (var per in item.PersonCities)
                {
                    var city=new CityDto(per.City.Name,per.CityId);
                    listCity.Add(city);
                }
                var person = new PersonDto(item.PersonId, item.Name, item.Surname, item.Nickname, listCity, item.PhoneNumber);
                list.Add(person);
            }

            return list;
        }

        // GET: api/People/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Person>> GetPerson(int id)
        {
            var person = await _context.Persons.FindAsync(id);

            if (person == null)
            {
                return NotFound();
            }

            return person;
        }

        // PUT: api/People/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPerson(int id, Person person)
        {
            if (id != person.PersonId)
            {
                return BadRequest();
            }

            int cityId = GetCityId(person);

            _context.Entry(person).State = EntityState.Modified;
            //need refac
            var pc = CreateCityPerson(person, cityId);
            _context.PersonsCities.Add(pc);
            _context.SaveChanges();
           

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PersonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/People
        [HttpPost]
        public async Task<ActionResult<Person>> PostPerson(Person person)
        {
            //_context.Persons.Add(person);
            //await _context.SaveChangesAsync();
            //return CreatedAtAction("GetPerson", new { id = person.PersonId }, person);
            int cityId = GetCityId(person);
            _context.Persons.Add(person);
            _context.SaveChanges();
            var pc = CreateCityPerson(person, cityId);
            _context.PersonsCities.Add(pc);
            _context.SaveChanges();
            return CreatedAtAction("GetPerson", new { id = person.PersonId }, person);
        }
        private int GetCityId(Person person)
        {
            var cityId = _context.Cities.FirstOrDefault(c => c.Name == person.City)?.CityId;
            return cityId.HasValue ? cityId.Value : Add(new City(person.City));
        }
        public int Add(City city)
        {
            var cityId = _context.Cities.FirstOrDefault(c => c.Name == city.Name)?.CityId;
            if (cityId == null)
            {
                _context.Cities.Add(city);
                _context.SaveChanges();
                return city.CityId;
            }

            return city.CityId;
        }
        private PersonCity CreateCityPerson(Person person, int cityId)
        {
            return new PersonCity() { CityId = cityId, PersonId = person.PersonId };
        }

        // DELETE: api/People/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Person>> DeletePerson(int id)
        {
            var person = await _context.Persons.FindAsync(id);
            if (person == null)
            {
                return NotFound();
            }
            _context.Remove(_context.Persons.Single(a => a.PersonId == id));
            _context.Remove(_context.PersonsCities.Single(a => a.PersonId == id));
            _context.SaveChanges();
            //_context.Persons.Remove(person);
            //await _context.SaveChangesAsync();
            return person;
        }

        private bool PersonExists(int id)
        {
            return _context.Persons.Any(e => e.PersonId == id);
        }
    }
}
