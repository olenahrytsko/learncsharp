﻿namespace DILearning
{
    public class Dep1
    {
        public InnerDep1 InnerDep1 { get; }

        public InnerDep2 InnerDep2 { get; }

        public Dep1(InnerDep1 innerDep1, InnerDep2 innerDep2)
        {
            InnerDep1 = innerDep1;
            InnerDep2 = innerDep2;
        }
    }
}