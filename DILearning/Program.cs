﻿using System;
using Autofac;

namespace DILearning
{
    class Program
    {
        //private static readonly container

        static void Main(string[] args)
        {
            var container = BuildContainer();
            //var a = new GodClass(new Dep1(new InnerDep1(), new InnerDep2()));
            var a = container.Resolve<GodClass>();
            a.DoWork();
            Console.ReadKey();
        }

        private static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<GodClass>();
            builder.RegisterType<Dep1>();
            builder.RegisterType<Dep2>();
            builder.RegisterType<InnerDep1>();
            builder.RegisterType<InnerDep2>();
            builder.RegisterType<InnerDep3>();
            return builder.Build();
        }
    }
}
