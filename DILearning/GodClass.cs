﻿using System;

namespace DILearning
{
    public class GodClass
    {
        public Dep1 Dep { get; }

        public Dep2 Dep2 { get; }

        public GodClass(Dep1 dep, Dep2 dep2)
        {
            Dep = dep;
            Dep2 = dep2;
        }

        public void DoWork()
        {
            Console.WriteLine("DI works!");
        }
    }
}