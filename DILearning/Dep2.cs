﻿namespace DILearning
{
    public class Dep2
    {
        public InnerDep1 InnerDep1 { get; }

        public InnerDep3 InnerDep3 { get; }

        public InnerDep2 InnerDep2 { get; }

        public Dep2(InnerDep1 innerDep1, InnerDep3 innerDep3, InnerDep2 innerDep2)
        {
            InnerDep1 = innerDep1;
            InnerDep3 = innerDep3;
            InnerDep2 = innerDep2;
        }
    }
}